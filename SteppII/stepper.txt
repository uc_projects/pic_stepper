LE1
LE2
Data
Clock

All Stepper motors have a upper limit on how many pulses the windings can 
handle per unit of time. To get an accurate estimate of how much time each 
winding should be energized take the maximum pulses per secound and divide
that number with a delay constant. Then you can delay X number of times
before sending another signal to the stepper motor.

 etc. 420PPS\1.04ms = ?


the Step angle is important to consider also. 360 degrees make a circle.
motors move in circles. if each step takes 1.8 degrees, then 360\1.8 = 
? steps to make a complete revolution.

To get an approximate rpm for a spinning stepper motor you need to know
the PPS, and the step angle.

1st find the number of steps for a complete rotation. 

	360\stepangle = #steps for 1 rpm = srpm.

2nd * the # steps for 1rpm by the time it takes 1 step to take place.

	srpm * delay = time for 1 rotation

So if you say you want 25RPM from a stepper motor with a 1.8 degree
step angle and it takes 100ms per step then it will require:

	360 \ 1.8 = #steps for 1 rotation = 200

	200 * .1 = time for 1 rotation = 20 sec.

The maximum # or RPM you can get from this motor is:

	60sec per min \ 20rps = 3rpm


To find the distance a motor turns with a single step you:

	2pi * the diameter the rotating shaft \ 1.8degrees

so when the shaft = 7\8" it moves (2Pi * 7\8) \ 1.8degrees