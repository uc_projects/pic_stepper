   title "Serial-ParallelConv"

; **********************************************************
; Functions
; Step (n)degrees, forward or reverse for m or mm
; Set Speed
;  * All functions are selectible for both motors,
; Function Register
;  7  6  5 4   3  2   1   0
; ID ID ID ID ID  RS  D   M
;
; M   : Motor Select, 1 for mm & 0 For m
; D   : Direction Select, 1 for Forward & 0 for reverse 
; RS  : Rotate\Set Speed, 1 for Rotate & 0 for Set Speed
; ID  = 16   
; ********************* Input Pins **************************
; DIn = GP0             
; ClkIn = GP3
; ****************** Output Pins ****************************
; MR = GP1              ; 74164 Pins           
; ClK = GP2
; D1 = GP4
; OE = GP5              ; 74573 Pin

; ***********-_-_-_-  -- Symbols  -- _-_--_--_-_-__--********
; Input Pins
DIn     equ 0             
ClkIn   equ 3

; Output Pins
CLK     equ 1             ; 74164 Pins
MR      equ 2
D1      equ 4
OE      equ 5             ; 74573 Pins

; Motor Reg. & function bits 
M       equ 0
D       equ 1
RS      equ 2
PM      equ 3 
Speed   equ 10
SpeedHi equ 11
Step0   equ 14
Step1   equ 15

; Recieve Reg.
Function equ 12
DataIn  equ 13

; Delay Reg. & Temp Reg.
Count   equ 7
CountHi equ 8
Temp    equ 9

; Setup mpasm for assembly
  LIST P=12C509  
  INCLUDE "P12C509a.inc"
 __CONFIG _CP_OFF & _WDT_OFF & _MCLRE_OFF & _IntRC_OSC

;  Program Start 
  PAGE

;  Mainline of Stepper
  org 0     
       movwf OSCCAL       ; -> factory osc. cal. value
       call  Init

; Mainline   391.51ms
Main   call  RSPin        ; read serial pin (498.13ms @ 4Mhz)

; *******************_-_-_-_-_-_-_-__*********************************
; This Routine Reads the RS232 Data Pin and saves the result to the DataIn reg. 
;
;  Bit Period = 1/Bits per secound
;	.0090909 - 110
;	.0033333 - 300
;	.0008333 - 1200
;	.0004166 - 2400
;	.0002083 - 4800
;	.00010416 - 9600
;	.00005283 - 19200
;	.0000260416 - 38400
;	.000017361 - 57600
;	.0000086805 - 115200
;	.0000043027 - 230400
;	.000002170138 - 460800
;	.0000010850694 - 921600
;
;________-____________-__-___-_________--__
;        Startbit       Data          StopBit
;         * 1bitlength   * 4-8bits     * 1,1.5,2bits
;                        * LSB-first
; Equates 
RSIn equ
Count equ
BitCount equ
DataIn

RSPin  movf   BitCount,W   ; recieve 8 bits
       movwf  Count,F           
       clrf   DataIn       
       ;bcf    STATUS,C

Rloop  btfss  GPIO,RSIn    ; Is Clock High?
        goto  $ - 1        ; If not, loop until it is                           
	
       btfsc  GPIO,RSIn    ; If so read data pin
         bsf  DataIn,0     ; & set bit accordingly
       
       decfsz Count,W      
          rrf DataIn,F     ; rotate the bit once for the next wright        
       decfsz Count,F      ; Did i recieve 8 bits
         goto Rloop        ; No i didn't                 
       return              ; Yep i'm done, so i'll do somthing else!
;**************************************************************
; This Routine Shifts Data into the 74164
Out   bsf   GPIO,OE       ; Disable 74573 outputs                 
      movlw 8
      movwf Count
O     btfsc Temp,7        ; is the bit set?
        bsf GPIO,D1       ; if so set D1 
       bsf  GPIO,CLK      ; Clock bit out
       nop   
       bcf  GPIO,CLK
       bcf  GPIO,D1
       rlf  Temp,F        ; rotate Temp If Count <= 7.       
       decfsz Count,F     ; Did we Shift All 8 bits out?   
         goto O           ; No, Loop Around
       bcf  GPIO,OE       ; Yes, Enable Outputs
       return
;**************************************************************
; Delay Routine

Dloop
dly  decfsz Count,F     ; delay lo
         goto dly0            
       decfsz CountHi,F ; delay hi
         goto Dloop                         
       return
;**************************************************************
; SET BIOS for Picmicro
Init
       movlw 199          ; disable GPWU, disable GPPU,
       option             ; TOCS on Fosc/4, inc on 1,                          

       clrf  GPIO      
       movlw 9            ; GP3 & GP0 are Inputs
       tris  GPIO         ; GP1,2,4,5 are Outputs        
       bsf   GPIO,MR      ; Shift Enable
       bsf   GPIO,OE      ; disable 74573 outputs 
       
       movlw 7            ; Initialize File Regs.
       movwf FSR
RamLp  clrf  INDF
       incf  FSR,F
       movf  FSR,W
       xorlw 224         
       btfss STATUS,Z     
        goto RamLp
       return
; *************************************************************** 
   end

        

