 title "StepIV"

;***********************************************************************
; Pinout:
;        __   __
;   V+ *|  \_/  |* GND
;  LE1 *|       |* 164_Data
;  LE0 *|12c672 |* 164_Clock
; RsIn *|_______|* RTS

; Hardware literals
D1      equ 0   ; 74164 Pins, Shared Data Line & clk lines
CLK0    equ 1
RTS	equ 2
RSIn	equ 3	; Rs232, Rx Input
LE0	equ 4	; 74573, Latch Enable
LE1	equ 5

Temp		equ	7
TempHi		equ	8
Count		equ	9
Flags		equ	10 ; Le0\1,XXX - Instruction bits, Motor bits

Shadow1640	equ	11
Shadow1641	equ	12

Instruction	equ	13
DataInLo	equ	14
DataInHi	equ	15

Instruction1	equ	16
DataInLo1	equ	17
DataInHi1	equ	18

Instruction2	equ	19
DataInLo2	equ	20
DataInHi2	equ	21

StepSequance0	equ	22
StepSequance1	equ	23
StepSequance2	equ	24
StepSequance3	equ	25
Speed0		equ	26
Speed1		equ	27
Speed2		equ	28
Speed3		equ	29

;30
;31

 include "p12c508.inc"  
 __CONFIG _CP_OFF & _WDT_OFF & _MCLRE_OFF & _IntRC_OSC


;DEBUG
;  Program Start
  PAGE
  org 0

;***********************************************************************
; Hardware initalize routines.
;-----------------------------------------------------------------------
Boot	movwf	OSCCAL	; -> factory osc. cal. value
;	movlw	71	; Enable GPWU, disable GPPU,
	movlw	199	; disable GPWU, disable GPPU,
	option          ; TOCS on Fosc/4, inc on 1

	clrf	GPIO
	movlw	48	; Set LE0&1
	movwf	GPIO
	movlw	8	; GP3 is only Input.
	tris	GPIO

	btfsc	STATUS,7; was reset caused by a pin change?
	 goto	Root

clram	movlw	7	; Clear File Regs.
	movwf	FSR
RamLp	clrf	INDF
	incf	FSR,F
	movf	FSR,W
	xorlw	224
	btfss	STATUS,Z
         goto	RamLp

	movlw	17
	movwf	Shadow1640
	movwf	Shadow1641

;***********************************************************************
; Root Loop
;-----------------------------------------------------------------------
Root	goto	RSRead - 2
exeins	movf	Instruction,W	; get lsb
	andlw	15
	movwf	Flags
	call	DecodeI

	movf	DataInHi,W
	movwf	DataInLo
	swapf	Instruction,W	; get msb
	andlw	15
	movwf	Flags
	call	DecodeI

exeins1	movf	Instruction1,W	; get lsb
	andlw	15
	movwf	Flags
	movf	DataInLo1,W
	movwf	DataInLo
	call	DecodeI

	movf	DataInHi1,W
	movwf	DataInLo
	swapf	Instruction1,W	; get msb
	andlw	15
	movwf	Flags
	call	DecodeI

exeins2	movf	Instruction2,W	; get lsb
	andlw	15
	movwf	Flags
	movf	DataInLo2,W
	movwf	DataInLo
	call	DecodeI

	movf	DataInHi2,W
	movwf	DataInLo
	swapf	Instruction2,W	; get msb
	andlw	15
	movwf	Flags
	call	DecodeI
	goto	Root

;***********************************************************************
; This Routine Reads RSPin and saves the result to the Temp reg.
;-----------------------------------------------------------------------
;  Bit Period = 1/Bits per secound
;	.0090909 - 110		.00005283 - 19200
;	.0033333 - 300		.0000260416 - 38400
;	.0008333 - 1200		.000017361 - 57600
;	.0004166 - 2400		.0000086805 - 115200
;	.0002083 - 4800
;	.00010416 - 9600
;
;-----------------------------------------------------------------------
; Rx pin 2.
;
; +3-25V	 _______  Data 	 _______		 _______
; 	  Idle	| Start	|  Bit0	|	|          Bit7	| Stop 	|
; -3-25V________|  Bit	|_______| Bit1	|_____....______|  Bit	|______
;
;-----------------------------------------------------------------------
;
;  	 (Dsub_Pin2-Rx)-----<R1-2.2KOhm>---+----------(GP3)
; +-----+-----+-------+		          _|_/
; | Rx  | GP3 |	State |	                 //_\  D1 - 5.1v >= 12ma
; +-----+-----+-------+			   |   25v\2.2K=11ma
; |-25v |-.7v |	  1   |			 __|__
; | 25v |4.5v |	  0   |			  GND
; +-----+-----+-------+
;-----------------------------------------------------------------------
	movlw	15
	movwf	FSR
RSRead	movlw	8		; recieve 8 bits
	movwf	Count		; @ 38,400 bps
	clrf	Temp

	btfss	GPIO,RSIn	; Is Clock High?
         goto	$ - 1		; If not, loop until it is.

	movlw	12
	movwf	TempHi
	decfsz	TempHi,F
	 goto	$ - 1

Rloop	btfss	GPIO,RSIn	; If so read data pin
         bsf	Temp,7		; & set bit accordingly

	goto	$ + 1
	goto	$ + 1
	goto	$ + 1

	decfsz	Count,W
         rrf	Temp,F		; rotate the bit once for the next wright
	decfsz	Count,F		; Did i recieve 8 bits
         goto	Rloop		; No i didn't

	movlw	19
	movwf	TempHi
	decfsz	TempHi,F
	 goto	$ - 1

	btfsc	GPIO,RSIn	; Check the Stop bit.
	 goto	RSRead		; start over

;-----------------------------------------------------------------------
	movf	Temp,W
	xorlw	"("		; Start transmission?
	btfss	STATUS,Z
	 goto	$ + 4
	movlw	13		; Instruction Address
	movwf	FSR
	goto	RSRead

	movf	Temp,W
	xorlw	")"		; Stop transmission?
	btfsc	STATUS,Z
	 goto	exeins

	movlw	237		; is FSR >= Instruction Address = 13?
	subwf	FSR,W
	btfss	STATUS,C
	 goto	RSRead

saveRS	movf	FSR,W		; is FSR = DataInLo2 Address + 1 = 22?
	xorlw	246
	btfsc	STATUS,Z
	 goto	RSRead		; yes, don't save!!!
	movf	Temp,W		; Rsin value
	movwf	INDF		; save Data!!!
	incf	FSR,F
	goto	RSRead


;***********************************************************************
; (W * 4, Temp = result) this routine converts W into the 1st step
; value address in the lookup table for that step sequance.
; values 0-23 for 24 different step sequances.
;-----------------------------------------------------------------------
tblLkup	andlw	b'00001111'	; mask out the msbits
	movwf	Temp		; save the result.
	movwf	TempHi		; and shadow it

	movlw	3
	movwf	Count

Lp	movf	TempHi,W
	addwf	Temp,F
	decfsz	Count,F
	 goto	Lp
	movf	Temp,W

;-----------------------------------------------------------------------
SeqTbl	addwf	PCL,F		; Step sequance lookup table.
	dt	1,2,4,8
	dt	1,2,8,4
	dt	1,4,2,8
	dt	1,4,8,2
	dt	1,8,2,4
	dt	1,8,4,2

	dt	2,1,4,8
	dt	2,1,8,4
	dt	2,4,1,8
	dt	2,4,8,1
	dt	2,8,1,4
	dt	2,8,4,1

	dt	4,1,2,8
	dt	4,1,8,2
	dt	4,2,1,8
	dt	4,2,8,1
	dt	4,8,1,2
	dt	4,8,2,1

	dt	8,1,2,4
	dt	8,1,4,2
	dt	8,2,1,4
	dt	8,2,4,1
	dt	8,4,1,2
	dt	8,4,2,1

;***********************************************************************
; Motor routines, step motor0-3 clockwise or cclockwise DataInLo X's
;-----------------------------------------------------------------------
; X-X-X-X-X-X-2-1      |  Decimal Val  |  Bit(s) Set |   motor #
; _____________________|_______________|_____________|____________
;     XXXX-XX00        =      0        =   none      =   motor 0
;     XXXX-XX01        =      1        =    1        =   motor 1
;     XXXX-XX10        =      2        =    2        =   motor 2
;     XXXX-XX11        =      3        =   0&1       =   motor 3
;-----------------------------------------------------------------------
StepM	movf	Shadow1640,W
	btfsc	Flags,1
	 movf	Shadow1641,W

	btfsc	Flags,0
	andlw	b'00001111'
	btfsc	Flags,0
	 goto	$ + 4
	andlw	b'11110000'
	movwf	Temp
	swapf	Temp,W

	call	ConvW

;-----------------------------------------------------------------------
	btfsc	Flags,2		; 1 = reverse
	 goto	dPort

iPort	incf	Temp,F		; Inc PORT0
	movf	Temp,W
	xorlw	4
	btfsc	STATUS,Z
	 clrf	Temp
	goto	StpSeq
	 
dPort	decf	Temp,F		; Dec Port0
	movf	Temp,W
	xorlw	255
	btfss	STATUS,Z
	 goto	StpSeq
	movlw	3
	movwf	Temp

;-----------------------------------------------------------------------
StpSeq	movf	Flags,W		; get Step Seq Reg.
	andlw	3
	addwf	PCL,F
	goto	Motor0
	goto	Motor1
	goto	Motor2

Motor3	movf	StepSequance3,W
	goto	aRSq
Motor2	movf	StepSequance2,W
	goto	aRSq
Motor1	movf	StepSequance1,W
	goto	aRSq
Motor0	movf	StepSequance0,W

aRSq	addwf	Temp,W		; get table value.
	call	tblLkup

;-----------------------------------------------------------------------
	btfsc	Flags,0		; Convert if Motor 1or3.
	 call	ConvTbl

	movf	Shadow1640,W	; get the other half to send.
	btfsc	Flags,1
	 movf	Shadow1641,W

	btfsc	Flags,0
	 andlw	b'11110000'
	btfss	Flags,0
	 goto	$ + 4
	andlw	b'00001111'
	movwf	TempHi
	swapf	TempHi,W

;-----------------------------------------------------------------------
	addwf	Temp,W
	movwf	Temp

	btfsc	Flags,1		; update shadow reg.
	 goto	$ + 3
	bcf	Flags,7
	movwf	Shadow1640

	btfss	Flags,1
	 goto	$ + 3
	movwf	Shadow1641
	bsf	Flags,7

;-----------------------------------------------------------------------
Out	movlw	8		; SRam write routine.
	movwf	Count

Outlp	btfsc	Temp,7		; is the bit set?
         bsf	GPIO,D1		; if so set D1

	bsf 	GPIO,CLK0
	bcf 	GPIO,CLK0

	bcf  	GPIO,D1
	rlf  	Temp,F		; rotate Temp If Count <= 7.
	decfsz 	Count,F		; Did we Shift All 8 bits out?
         goto 	Outlp		; No, Loop Around

	btfss	Flags,7		; & latch data.
	 goto	$ + 4
	bcf  	GPIO,LE1
	bsf 	GPIO,LE1
	goto	$ + 3
	bcf 	GPIO,LE0
	bsf 	GPIO,LE0	;...

;-----------------------------------------------------------------------
	decfsz	DataInLo,F
	 goto	StepM
	retlw	0

;***********************************************************************
; Decode XXXX-00XX in W reg, goto N :with i & m bits in W.
;-----------------------------------------------------------------------
; X-X-X-X-8-4-X-X      |  Decimal Val  |  Bit(s) Set | goto label
; _____________________|_______________|_____________|____________
;     XXXX-00XX        =      0        =   none      =   SetSpd
;     XXXX-01XX        =      4        =    3        =   SetSeq
;     XXXX-10XX        =      8        =    4        =   StepMF
;     XXXX-11XX        =      12       =   3&4       =   StepMR
;
DecodeI	btfsc	Flags,3
	 goto	StepM

	btfsc	Flags,2
	 goto	SetSeq

;-----------------------------------------------------------------------
SetSpd	movf	Temp,W
	andlw	3
	addwf	PCL,F
	goto	SetSpd0
	goto	SetSpd1
	goto	SetSpd2

SetSpd3	movf	DataInLo,W
	movwf	Speed3
	retlw	0

SetSpd2	movf	DataInLo,W
	movwf	Speed2
	retlw	0

SetSpd1	movf	DataInLo,W
	movwf	Speed1
	retlw	0

SetSpd0	movf	DataInLo,W
	movwf	Speed0
	retlw	0

;-----------------------------------------------------------------------
SetSeq	movf	Temp,W
	andlw	3
	addwf	PCL,F
	goto	SetSeq0
	goto	SetSeq1
	goto	SetSeq2

SetSeq3	movf	DataInLo,W
	movwf	StepSequance3
	retlw	0

SetSeq2	movf	DataInLo,W
	movwf	StepSequance2
	retlw	0

SetSeq1	movf	DataInLo,W
	movwf	StepSequance1
	retlw	0

SetSeq0	movf	DataInLo,W
	movwf	StepSequance0
	retlw	0

;***********************************************************************
; convert the lsnybble in W into a # 0-3
;-----------------------------------------------------------------------
ConvW	movwf	Temp
	btfsc	Temp,0
	 movlw	0
	btfsc	Temp,1
	 movlw	1
	btfsc	Temp,2
	 movlw	2
	btfsc	Temp,3
	 movlw	3
	movwf	Temp
	retlw	0

;***********************************************************************
; This Routine converts the lsbyte table values to msbyte values for
; motors 2 and 4.
; W = table generated value, and the result is saved to Temp.
;-----------------------------------------------------------------------
ConvTbl	movwf	Temp
	movlw	4
	movwf	Count

Convlp	movf	Temp,W
	addwf	Temp,F
	decfsz	Count,F
	 goto	Convlp
	retlw	0

;***********************************************************************
 end
