   subtitle "Seiko_c550001-LCD3LINE - Control an LCD using 3 Lines."
; *******************************************************************************
; Execute an LCD instruction, display a character, + Shift Register
;
; Routine:              LCDIns          LCDChar         LCDInit
; Display Characters       0               1               0
; Instructions used:       41              31              5
; Registers used:       LCDTemp         LCDTemp
;-----------------------------------------------------------------------
;
;  Myke Predko		Brad Steffy
;  97.01.23              01.09.12
;
;  IDE CONNECTOR:
;        _____		1)vled  5)GND  9)D7
;	|5  10|		2)D4    6)V+   10)E
;       |4   9|		3)D6    7)RW
;     |||3   8|		4)RS    8)D5
;	|2   7|
;	|1   6|
;        -----
;
; The 3 line Serial interface reduces the # of I\O Pins used to control
; the LCD, and demonstrates how a SIPO shift register can be used
; to expand Output ports.
;
;                                                      (1-4---+ 
;                  +-----------------------------E-----(5     |
;------------      |                                +--(6     |
;   PIC     |      |                                |  (7-10--+
;           |      |          _______  Bit          |         |
;       RB0 |)-----+         | Shift |)-4--------RS-+         |
;           |                | Reg   |)-3--------D7----(11    |
;           |                |       |)-2--------D6----(12    |
;       RA1 |)--------------(|CLK    |)-1--------D5----(13    |
;       RA0 |)--------------(|D1     |)-0--------D4----(14    |
;___________|                |_______|                 (15-RW-+
;                                                             |
;                                                Vlcd--(16<R1>+
;                                      +--<R2>-- Vled--(17    |		
;                                      +---------Vdd---(18    |
;	                              _|_              (19-22-+
;                                      V+                     |
;                                                           __|__
;                                                            GND
;
; Notes:
;  * The SIPO Shift Register must be 5 bits or more.
;  * <R2> = 100-150 ohms.
;  * Before the Initialization is to be executed by the PIC, 15 msecs must
;     elapse before the "Init" subroutine is called.
;
; *******************************************************************************
;  Software Notes:
;
;E		; LCD clock pin
;
;LCDTemp	; I\O Reg
;DTemp		; Disp Regs
;DTempHi
;DCount
;
;SOut		; Shift Reg Routine
;Dlay160	; required delay routines
;Dlay5ms
;
; *******************************************************************************
;LCD ADDRESSES\PAGES
; **************************************************
; *00*01*02*03*04*05*06*07**08*09*10*11*12*13*14*15*	Page1
; *64*65*66*67*68*69*70*71**72*73*74*75*76*77*78*79*
; **************************************************
;
; **************************************************
; *16*17*18*19*20*21*22*23**24*25*26*27*28*29*30*31*    Page2
; *80*81*82*83*84*85*86*87**88*89*90*91*92*93*94*95*
; **************************************************
;
; *********************************
; *32 *33 *34 *35 *36 *37 *38 *39 *	Page 2.5
; *96 *97 *98 *99 *100*101*102*103*
; *********************************
;
; *******************************************************************************
DecodeL	addwf	PCL,F
        retlw   b'11111'	; Custom Char0
        retlw   b'10001'
        retlw   b'11101'
        retlw   b'11001'
        retlw   b'10111'
        retlw   b'10001'
	retlw	b'11111'
	retlw	b'11111'	; cursor

        retlw   b'11111'	; Custom Char1
        retlw   b'10001'
        retlw   b'11101'
        retlw   b'10001'
        retlw   b'11101'
        retlw   b'10001'
	retlw	b'11111'
	retlw	b'11111'	; cursor

Eclk	bsf	PORTA,E			;  Toggle the 'E' Clock
	bcf	PORTA,E	
	return

; Initialize LCD
LCDInit	movlw	2
	call	SOut
	call	Eclk
	call	Dlay5ms			;  Wait for the LCD to Power Up
	movlw	2
	call	SOut
	call	Eclk
	call	Dlay5ms

	movlw	8
	call	SOut
	call	Eclk
	call	Dlay5ms
	movlw	0
	call	SOut
	call	Eclk
	call	Dlay5ms

	movlw	14
	call	SOut
	call	Eclk
	call	Dlay5ms
	movlw	6
	call	LCDIns

; There are 0-7 custom chars available.
; The 1st address = CustChar(0-7) * 8
	movlw	15
	movwf	DCount
	clrf	DTemp
	movlw	64		; Chr addr + 64
	call	LCDIns		; set CGRAM address

custchr	movf	DTemp,W
	call	DecodeL	
	call	LCDChar
	incf	DTemp,F
	decfsz	DCount,F
	 goto	custchr

	movlw	128		; Set DDRAM Address
	call	LCDIns
	call	ClrDisp
	return

; *******************************************************************************
LCDspac	movlw	' '
	goto	LCDChar

; Get Decimal Value and return W with ASCII Char
DispNum addlw	48
	movwf	LCDTemp
	sublw	58
	btfsc	STATUS,Z
	 bcf	STATUS,C
	btfsc	STATUS,C
	 goto	LCDChar + 1
	movlw	7
	addwf	LCDTemp,F
	goto	LCDChar + 1

; Send the Byte as Character Data
LCDChar	movwf	LCDTemp			;  Save the Value for the Second Nybble
	swapf	LCDTemp,W		;  Get the High Nybble to Send
	andlw	15
	addlw	16			; Make RS=1
	call	SOut
	call	Eclk

	movf	LCDTemp,W		; get lo nibble
	andlw	15
	addlw	16			; Make RS=1
	call	SOut
	call	Eclk
	call	Dlay160			;  Wait for the LCD to Display the Character
	return

; ********** LCD Instructions ***************************************************
ShftD	addlw	16		; W = 1 for right shift,
	goto	LCDIns		; W = 0 for left shift.

ClrDisp	movlw	1
	goto	LCDIns

RetHome	movlw	2
	goto	LCDIns

LcdAd	addlw	128		; Send Cursor to addr.

; Send the Byte as an Instruction
LCDIns	movwf	LCDTemp
	swapf	LCDTemp,W		;  Get the High Nybble to Send
	andlw	15
	call	SOut
	call	Eclk

	movf	LCDTemp,W
	andlw	15
	call	SOut
	call	Eclk

	call	Dlay160			;  Wait for the LCD to Process the Instruction
	movlw	0x0FC			;  "Clear Display" and "Cursor At Home" Instructions
	andwf	LCDTemp,W		;   Require 5 msec Delay to Complete
	btfsc	STATUS,Z
	call	Dlay5ms
	return

; *******************************************************************************
; Display the W register in binary, decimal, or hexedecimal characters.
;
; Routine:              DispDec         DispHex         DispBin
; Display Characters       3		   2		   8
; Instructions used:       55              8               11
; Registers used:       DTemp                            DTemp
;                       DTempHi           DTemp          DCount
;                       DCount
;-----------------------------------------------------------------------
                                           
DispDec movwf   DTemp           ; "001" - "255" is displayed
      	clrf   	DTempHi
      	movlw  	2
      	movwf  	DCount

LBCD  	movlw  	100
      	subwf  	DTemp,W		; W = DTemp - 100
      	btfss  	STATUS,C	; if C = 1 then DTemp>=100
       	 goto   TBCD		; if C = 0 then DTemp<100
      	movwf  	DTemp		; Store DTemp Reg.
      	incf   	DTempHi,F	; Update 100Count
      	btfsc  	STATUS,Z	; If Z = 1 then DTemp=100
      	 goto  	TBCH		; DTemp = 100
      	decfsz 	DCount,F	; is DTemp>100?
      	 goto  	LBCD       	; Yes
TBCD	movf   	DTempHi,W	; No Print to LCD
        call    DispNum

      	movlw  	9
      	movwf  	DCount
      	clrf   	DTempHi
LBCE  	movlw  	10             
      	subwf  	DTemp,W     	; W = DTemp - 10
      	btfss  	STATUS,C   	; if C = 1 then DTemp>=10
       	 goto  	TBCE       	; if C = 0 then DTemp<10
      	movwf  	DTemp       	; Store DTemp Reg.
      	incf   	DTempHi,F 	; Update 10Count
      	btfsc  	STATUS,Z   	; If Z = 1 then DTemp=10
       	 goto  	TBCI       	; DTemp = 10
      	decfsz 	DCount,F	; is DTemp>10?
       	 goto  	LBCE		; Yes
TBCE	movf   	DTempHi,W	; No Print to LCD
        call    DispNum
      	movf   	DTemp,W
      	btfsc  	STATUS,Z	; Does Temp=0?
       	 goto  	TBCF		; Yes

      	clrf   	DTempHi
LBCF  	decfsz 	DTemp,F		; is DTemp>1?
      	 goto  	TBCG		; Yes
      	incf   	DTempHi,W	; No, DTemp = 1
TBCF  	call   	DispNum		; Print to LCD
      	return			; Finished

TBCG	incf	DTempHi,F	; DTemp>1  
      	 goto   LBCF       
TBCH  	movf  	DTempHi,W
      	call   	DispNum		; Print to Lcd
      	movlw  	'0'		; Print the leading 0's
      	call   	LCDChar
        movlw   '0'
      	call   	LCDChar
      	return			; Finished

TBCI	movf  	DTempHi,W
        call    DispNum		; Print to Lcd
      	movlw  	'0'		; Print the leading 0
      	call   	LCDChar
      	return			; Finished

; *******************************************************************************
DispHex movwf   DTemp           ; "00" - "FF" is displayed
	swapf	DTemp,W	; Disp MSN
	andlw	15
	call	DispNum

	movf	DTemp,W	; Disp LSN
	andlw	15
	call	DispNum
	return

; *******************************************************************************
DispBin movwf   DTemp           ; MSB "00000000"  - "11111111" LSB is displayed
	movlw	8
      	movwf	DCount

BinLp   movlw	0
      	btfsc	DTemp,7
       	 movlw	1
      	call	DispNum		; Disp Bit
	rlf	DTemp,F
      	decfsz	DCount,F
       	goto	BinLp
      	return
