; *******************************************************************************
; subtitle  "Serial Serial 9600 bps Interrupt Rx/Tx."
; *******************************************************************************
;  These Routines are used to provide a Serial (Digital Logic)
;   from a PIC.  These routines do not use the Serial communications
;   hardware in some PICs and does require interrupts.  Therefore,
;   The 16C61, 16C71, and 16C84 are the recommended target devices.
;
;  Note that the input is DIGITAL LOGIC.  For RS-232 "Cheats", the
;   values Read/Output will have to be inverted.
;
;  Currently they are setup for a PIC running at
;   3.579545 MHz (Colour Burst Frequency) and 9600 bps.  This
;   can be changed by changing the count value.
;
;  Note that the Received Data is put into the 5 byte circular
;   "Buffer".
;
;  It is assumed that the host the PIC running this code can use
;   a simple 3 wire interface.
;
;  Hardware Notes:
;   The Pic is Running with a 3.579545 MHz (Colour Burst) Crystal for 16C84
;   RA1 is Connected to RS-232 "TX" Line
;   RB0 is Connected to RS-232 "RX" Line
;  NOTE:  RA1 is used to Transmit data rather than RA0 because
;   RA0 is usually used to look like the LSB of PORTB.
;
;  Myke Predko
;  96.09.22
;
; Bit     EQU     27,12,4         ;  Delay between the Bits, using the
; BitStart EQU    Bit / 3         ;  Delay to check in character middle
;
;  The Bit Count is Calculated by the Formula:
;
;   Period = 1 / bps            ;  Get the Period required.
;   Total Cycles = ( Period * ( PIC_Frequency / 4 )) - 15
;                               ;  Number of Cycles to Execute
;   Bit = ( Total_Cycles / 3 ) + 1
;                               ;  The actual number of Dlay Loops
;
;  For Example: Getting 2400 bps at 4 MHz:
;   Period = 1 / 2400 = 0.0004167
;   Total Cycles = ( 4.167(10^-4) * 4(10^6) / 4 ) - 15 = 402
;   Bit = ( 402 / 3 ) + 1 = 135
;
;
; *******************************************************************************
;  subtitle "Seiko_c550001-LCD3LINE - Control an LCD using 3 Lines."
; *******************************************************************************
; Execute an LCD instruction, display a character, + Shift Register
;
; Routine:              LCDIns          LCDChar         LCDInit
; Display Characters       0               1               0


; Instructions used:       41              31              5
; Registers used:       LCDTemp         LCDTemp
;-----------------------------------------------------------------------
;
;  Myke Predko		Brad Steffy
;  97.01.23              01.09.12
;
;  IDE CONNECTOR:
;        _____		1)vled  5)GND  9)D7
;	|5  10|		2)D4    6)V+   10)E
;       |4   9|		3)D6    7)RW
;     |||3   8|		4)RS    8)D5
;	|2   7|
;	|1   6|
;        -----
;
; The 3 line Serial interface reduces the # of I\O Pins used to control
; the LCD, and demonstrates how a SIPO shift register can be used
; to expand Output ports.
;
;                                                      (1-4---+ 
;                  +-----------------------------E-----(5     |
;------------      |                                +--(6     |
;   PIC     |      |                                |  (7-10--+
;           |      |          _______  Bit          |         |
;       RB0 |)-----+         | Shift |)-4--------RS-+         |
;           |                | Reg   |)-3--------D7----(11    |
;           |                |       |)-2--------D6----(12    |
;       RA1 |)--------------(|CLK    |)-1--------D5----(13    |
;       RA0 |)--------------(|D1     |)-0--------D4----(14    |
;___________|                |_______|                 (15-RW-+
;                                                             |
;                                                Vlcd--(16<R1>+
;                                      +--<R2>-- Vled--(17    |		
;                                      +---------Vdd---(18    |
;	                              _|_              (19-22-+
;                                      V+                     |
;                                                           __|__
;                                                            GND
;
; Notes:
;  * The SIPO Shift Register must be 5 bits or more.
;  * <R2> = 100-150 ohms.
;  * Before the Initialization is to be executed by the PIC, 15 msecs must
;     elapse before the "Init" subroutine is called.
;
; *******************************************************************************
;  Software Notes:
;
;E		; LCD clock pin
;
;LCDTemp	; I\O Reg
;DTemp		; Disp Regs
;DTempHi
;DCount
;
;SOut		; Shift Reg Routine
;Dlay160	; required delay routines
;Dlay5ms
;
; *******************************************************************************
;LCD ADDRESSES\PAGES
; **************************************************
; *00*01*02*03*04*05*06*07**08*09*10*11*12*13*14*15*	Page1
; *64*65*66*67*68*69*70*71**72*73*74*75*76*77*78*79*
; **************************************************
;
; **************************************************
; *16*17*18*19*20*21*22*23**24*25*26*27*28*29*30*31*    Page2
; *80*81*82*83*84*85*86*87**88*89*90*91*92*93*94*95*
; **************************************************
;
; *********************************
; *32 *33 *34 *35 *36 *37 *38 *39 *	Page 2.5
; *96 *97 *98 *99 *100*101*102*103*
; *********************************
;
; *******************************************************************************
;* PortB                       		      V+
;* 5 Coulmn 2 -------------------+-------/\/---|  internal pullup
;* 4 Coulmn 1 -------------+-----|-------/\/---|   resistors
;* 3 Coulmn 0 -------+-----|-----|-------/\/---|
;*                   |     |     |
;* 74164             |     |     |
;* 7  Row 3 --|<---[3,5]-[3,6]-[2,5]
;* 6  Row 2 --|<---[1,4]-[1,5]-[0,4]
;* 5  Row 1 --|<---[1,3]-[0,1]-[1,1]
;           1N914
;
; 852  key assignment
; 963
; 741
;
; *******************************************************************************
DecodeL	addwf	PCL,F
        retlw   b'11111'	; Custom Char0
        retlw   b'10001'
        retlw   b'11101'
        retlw   b'11001'
        retlw   b'10111'
        retlw   b'10001'
	retlw	b'11111'
	retlw	b'11111'	; cursor

RxTable	addwf	PCL,F
	 dt "BPS = "
	 dt "9600 "
	 dt "19200"

LUT_RS0	movf	Temp,W
	call	RxTable
	call	LCDChar
	incf	Temp,F
	decfsz	Count,F
	 goto	LUT_RS0
	return

; *******************************************************************************
;  RX Interrupt Handler
;  Read Serial Routine - Read Char, Put in Buffer, return to Loop
; *******************************************************************************
ReadRS232                       ;  Start of Original Serial Read Routine
  bcf    STATUS, RP0            ;  Make Sure we're in Bank 1
  decfsz BitCount               ;  Wait for Halfway through the Character
   goto  $ - 1

  btfss  PORTB, 0               ;  Do we *Still* have the Start Bit?
   goto  ShortRRLoop            ;  Point of Inversion depending on Logic
  goto   RREnd                  ;  Wait for the Next Transition

ShortRRLoop                     ;  Now, Loop Around Here for Each Character
  movf  Bit,W                ;  Load and wait for the Bit Count
  goto	$ + 1
  goto	$ + 1
  goto  $ + 1
  movwf  BitCount
  decfsz BitCount               ;  Now Dlay
   goto  $ - 1

  rrf    PORTB, w               ;  Get the Bit Value Coming in
  rrf    Char                   ;  Update the Character

  decfsz RRCount                ;  Do 8x
   goto  ShortRRLoop

  movf   Bit,W                  ;  Now, Look at the Stop Bit
  movwf  BitCount
  decfsz BitCount
   goto  $ - 1

  btfss  PORTB, 0               ;  Now, Check to see if the Stop Bit is There
   goto  RREnd                  ;  It's not, Ignore the Character

HaveRS232                       ;  We Have Correct RS232 Value - End and Return
  incf   Next                   ;  Now, Store the Character in the Buffer

  movlw  BufferEnd + 1          ;  Are we at the End of the Buffer?
  subwf  Next, w
  btfsc  STATUS, C              ;  If Carry Set, No
   movlw BufferStart - ( BufferEnd + 1 )  ;  Else, have to Roll to Start
  addlw  BufferEnd + 1          ;  Reset the Start of Next

  xorwf  FSR, w                 ;  Swap Next with FSR (Shadow Contains Current FSR)
  xorwf  FSR
  xorwf  FSR, w
  movwf  Next                   ;  Save the Old FSR Value

  movf   Char, w                ;  Store the Character in the Buffer
  movwf  INDF

  movf   Next, w                ;  Restore the FSR
  xorwf  FSR, w
  xorwf  FSR
  xorwf  FSR, w
  movwf  Next                   ;  Store the Current Next Value

RREnd                           ;  Return to where the Routine was Called
;  movlw  intreg & 127		;  Interrupts
;  movwf  INTCON
  bcf	INTCON,INTF
  movlw  8                      ;  Reload the Bit Counter for the Loop
  movwf  RRCount

  movf   BitStart,W             ;  Reload the Counters for the Next Serial Character
  movwf  BitCount
  return

; *******************************************************************************
;  Character Read Routine.  Wait for Something to be Available
; *******************************************************************************
GetCHAR
  movf   ShadowFSR, w           ;  Get the Last Character Read
  subwf  Next, w                ;   in the Buffer and If it
  btfsc  STATUS, Z              ;   hasn't been read - return it
   retlw 0                	;  Else, Return

  incf   ShadowFSR, w           ;  Point to the Next Value in
  addlw  0 - ( BufferEnd + 1 )  ;   the Table
  btfsc  STATUS, C
   addlw BufferStart - ( BufferEnd + 1 )
  addlw  BufferEnd + 1
  movwf  ShadowFSR              ;  Save it for the Next Read

  movwf  FSR                    ;  Get the newly entered Character
  movf   INDF, w
  return

; *******************************************************************************
;  Character Send Routine - Send the Character in "w"
; *******************************************************************************
SendCHAR                        ;  Serial Send the Character to the Host
 ifndef DEBUG
  movwf  Temp                   ;  Save the Character to Send

  movlw  8                      ;  Setup the 8 Bits to Send
  movwf  SCCount
  bcf    PORTA, 1               ;  Output the Start Bit

SCLoop                          ;  Loop Here for Each Character
  movlw  Bit                    ;  Delay the Length of the Bit
  movwf  Count
  decfsz Count
   goto  $ - 1

  rrf    Temp                   ;  Rotate the Bit Over
  btfsc  STATUS, C              ;  Do we have to Send a '1'?
   goto  SCOne

  nop                           ;  Send a Zero
  bcf    PORTA, 1               ;  NOTE: it is Inverted!
  goto   SCLoopEnd

SCOne                           ;  Send a One
  bsf    PORTA, 1               ;  NOTE: it is Inverted!
  goto   $ + 1                  ;  Delay 2 cycles to Even Up

SCLoopEnd                       ;  Do this for 8 Bits
  nop                           ;  To Time Everything Out
  decfsz SCCount
   goto  SCLoop

  nop                           ;  Keep everything on track
  movlw  Bit                    ;  Delay for the Bit
  movwf  Count
  goto   $ + 1                  ;  Delay the Correct Number of Cycles
  goto   $ + 1
  decfsz Count
   goto  $ - 1

  bsf    PORTA, 1               ;  Send a Zero Stop Bit
  nop
  movlw  Bit                    ;  Wait the Correct Number of Cycles before Returning
  movwf  Count
  decfsz Count
   goto  $ - 1
 endif
  return

; *******************************************************************************
;  Display Routines
; *******************************************************************************
Eclk	bsf	PORTA,E			;  Toggle the 'E' Clock
	bcf	PORTA,E	
	return

; *******************************************************************************
LCDspac	movlw	' '
	goto	LCDChar

; Get Decimal Value and return W with ASCII Char
DispNum addlw	48
	movwf	LCDTemp
	sublw	58
	btfsc	STATUS,Z
	 bcf	STATUS,C
	btfsc	STATUS,C
	 goto	LCDChar + 1
	movlw	7
	addwf	LCDTemp,W

; Send the Byte as Character Data
LCDChar	movwf	LCDTemp			;  Save the Value for the Second Nybble
  ifndef DEBUG
	swapf	LCDTemp,W		;  Get the High Nybble to Send
	addlw	16			; Make RS=1
	call	SOut
	call	Eclk

	movf	LCDTemp,W		; get lo nibble
	addlw	16			; Make RS=1
	call	SOut
	call	Eclk
	call	Dlay160			;  Wait for the LCD to Display the Character
  endif
	return

; ********** LCD Instructions ***************************************************
ClrDisp	movlw	1
	goto	LCDIns
Line1	movlw	2	; Return home.
	goto	LCDIns
Line2	movlw	64
LcdAd	addlw	128		; Send Cursor to addr.

; Send the Byte as an Instruction
LCDIns
  ifndef DEBUG
	movwf	LCDTemp
	swapf	LCDTemp,W	; Get the High Nybble to Send
	andlw	15
	call	SOut
	call	Eclk

	movf	LCDTemp,W
	andlw	15
	call	SOut
	call	Eclk

	call	Dlay160		;  Wait for the LCD to Process the Instruction
	movlw	0x0FC		;  "Clear Display" and "Cursor At Home" Instructions
	andwf	LCDTemp,W	;   Require 5 msec Delay to Complete
	btfsc	STATUS,Z
	call	Dlay5ms
    endif
	return

; *******************************************************************************
; Display the W register in binary, decimal, or hexedecimal characters.
;
; Routine:              DispDec         DispHex         DispBin
; Display Characters       3		   2		   8
; Instructions used:       55              8               11
; Registers used:       DTemp             DTemp          DTemp
;                       DTempHi                          DCount
;                       DCount
;-----------------------------------------------------------------------                                           
DispDec
  ifndef DEBUG
	movwf   DTemp           ; "001" - "255" is displayed
      	clrf   	DTempHi
      	movlw  	2
      	movwf  	DCount

LBCD  	movlw  	100
      	subwf  	DTemp,W		; W = DTemp - 100
      	btfss  	STATUS,C	; if C = 1 then DTemp>=100
       	 goto   TBCD		; if C = 0 then DTemp<100
      	movwf  	DTemp		; Store DTemp Reg.
      	incf   	DTempHi,F	; Update 100Count
      	btfsc  	STATUS,Z	; If Z = 1 then DTemp=100
      	 goto  	TBCH		; DTemp = 100
      	decfsz 	DCount,F	; is DTemp>100?
      	 goto  	LBCD       	; Yes
TBCD	movf   	DTempHi,W	; No Print to LCD
        call    DispNum

      	movlw  	9
      	movwf  	DCount
      	clrf   	DTempHi
LBCE  	movlw  	10             
      	subwf  	DTemp,W     	; W = DTemp - 10
      	btfss  	STATUS,C   	; if C = 1 then DTemp>=10
       	 goto  	TBCE       	; if C = 0 then DTemp<10
      	movwf  	DTemp       	; Store DTemp Reg.
      	incf   	DTempHi,F 	; Update 10Count
      	btfsc  	STATUS,Z   	; If Z = 1 then DTemp=10
       	 goto  	TBCI       	; DTemp = 10
      	decfsz 	DCount,F	; is DTemp>10?
       	 goto  	LBCE		; Yes
TBCE	movf   	DTempHi,W	; No Print to LCD
        call    DispNum
      	movf   	DTemp,W
      	btfsc  	STATUS,Z	; Does Temp=0?
       	 goto  	TBCF		; Yes

      	clrf   	DTempHi
LBCF  	decfsz 	DTemp,F		; is DTemp>1?
      	 goto  	TBCG		; Yes
      	incf   	DTempHi,W	; No, DTemp = 1
TBCF  	call   	DispNum		; Print to LCD
      	return			; Finished

TBCG	incf	DTempHi,F	; DTemp>1  
      	 goto   LBCF       
TBCH  	movf  	DTempHi,W
      	call   	DispNum		; Print to Lcd
      	movlw  	'0'		; Print the leading 0's
      	call   	LCDChar
        movlw   '0'
      	call   	LCDChar
      	return			; Finished

TBCI	movf  	DTempHi,W
        call    DispNum		; Print to Lcd
      	movlw  	'0'		; Print the leading 0
      	call   	LCDChar
  endif
      	return			; Finished

; *******************************************************************************
DispHex
  ifndef DEBUG
	movwf   DTemp           ; "00" - "FF" is displayed
	swapf	DTemp,W	; Disp MSN
	andlw	15
	call	DispNum

	movf	DTemp,W	; Disp LSN
	andlw	15
	call	DispNum
  endif
	return

; *******************************************************************************
DispBin
  ifndef DEBUG
	movwf   DTemp           ; MSB "00000000"  - "11111111" LSB is displayed
	movlw	8
      	movwf	DCount

BinLp   movlw	0
      	btfsc	DTemp,7
       	 movlw	1
      	call	DispNum		; Disp Bit
	rlf	DTemp,F
      	decfsz	DCount,F
       	goto	BinLp
  endif
      	return

; *******************************************************************************
TestKey	movlw	b'00011011'	; Test 4 a key press!
	call	SOut

	movf	PORTB,W		; Get Rows
	andlw	b'00111000'	;  = 112

; IF W < 112 then a key is pressed.
	sublw	b'00111000'	; 112 - W
	btfsc	STATUS,Z	; If W = 112 then no key pressed.
	 retlw	0

	btfss	STATUS,C
	 retlw	0		; W can't be > 112!!!
	retlw	1		; Key Pressed

; ******************************************************************************************   
; This Macro waits for a PORT Pin to reach a set state for a set time before continuing. 
; TimeDelay = (((InitDlay - 1 ) * 256) * 7) / (Frequency / 4)                    
;                     or                        
;  InitDlay = ((TimeDelay * (Frequency / 4)) / (256 * 7)) + 1
;   This means that with an "InitDlay" value of 12, a 19.7 msec
;    debounce delay will be produced for a PICMicro running at 4 MHz.
Lo		equ 0	; Macro Assign.
InitDlay	equ 6   ; Macro Dly time
Bit0		equ 0   ; Bit assignments for Debounce Macro
Bit1		equ 1
Bit2		equ 2
Bit3		equ 3
Bit4		equ 4
Bit5		equ 5
Bit6		equ 6
Bit7		equ 7

Debounce macro HiLo, Port, Bit
	if HiLo == Lo
	 btfss Port,Bit	       ;  Is the Button Pressed?
	else
	 btfsc Port,Bit
	endif
	 goto $ - 1            ;   Yes - Wait for it to be Released
	 movlw InitDlay        ;  Wait for Release to be Debounced
	 movwf dly_Lo          ;  Have to Delay 20 msecs
	 movlw 0
	if HiLo == Lo
	 btfss Port,Bit        ;  If Button Pressed, Wait Again for it
	else
	 btfsc Port,Bit
	endif
	 goto $ - 6            ;   to be Released
	ifndef Debug           ;  Skip Small Loop if "Debug" Defined
 	 addlw 1               ;  Increment the Delay Count
 	 btfsc STATUS, Z       ;   Loop If Low Byte (w) Not Equal to Zero
	else
	 nop                   ;  Match the Number of Instructions
	 nop
	endif
	 decfsz dly_Lo,f
	goto $ - 5
 endm

; *******************************************************************************
keypad
  ifndef DEBUG
	movlw	b'11011100'	; Test 4 a key press!
	call	SOut
	call	Dlay160
	movlw	3		; 3 columns
	movwf	DCount
	movlw	1
	movwf	DTemp

Row0	btfsc	PORTB,3
	 goto	Row1
	Debounce 0, PORTB, Bit3
	movf	DTemp,W
	return
Row1	incf	DTemp,F
	btfsc	PORTB,4
	 goto	Row2
	Debounce 0, PORTB, Bit4
	bsf	INDF,1
	movf	DTemp,W
	return
Row2	incf	DTemp,F
	btfsc	PORTB,5
	 goto	Rlp
	Debounce 0, PORTB, Bit5
	movf	DTemp,W
	return

Rlp	incf	DTemp,F
	bsf	PORTB,Clk	; rlf 74164,F
	bcf	PORTB,Clk
	call	Dlay160
	decfsz	DCount,F
	 goto	Row0
  endif
	retlw	0

; *******************************************************************************
LinitLp	call	SOut
	call	Eclk
	call	Dlay5ms
	return

; Initialize LCD
LCDInit	movlw	2
	call	LinitLp		;  Wait for the LCD to Power Up
	movlw	2
	call	LinitLp
	movlw	8
	call	LinitLp
	movlw	0
	call	LinitLp
	movlw	6
	call	LCDIns

; There are 0-7 custom chars available.
; The 1st address = CustChar(0-7) * 8
	movlw	64		; Chr addr + 64
	call	LCDIns		; set CGRAM address

	movlw	7		; store 8 bytes
	movwf	Count
LUT_LCD	movf	Temp,W
	call	DecodeL	
	call	LCDChar
	incf	Temp,F
	decfsz	Count,F
	 goto	LUT_LCD

	movlw	128		; Set DDRAM Address
	call	LCDIns
	call	ClrDisp

; --------------------------------------------------------------------
RXSetup	movlw	9
	movwf	Count
	movlw	0
	call	LUT_RS0

	movlw	4		; Display 5 characters.
	movwf	Count
RXKey	call    keypad          ; check for key press.
	movwf	DTemp
	xorlw	5
	btfsc	STATUS,Z
	 goto	Rx19200

	movf	DTemp,W
	xorlw	2
	btfsc	STATUS,Z
	 goto	Rx9600
	goto	RXKey
	
Rx9600	movlw	27
	movwf	Bit
	movlw	9
	movwf	BitStart
	goto	RxBaud + 1

Rx19200	movlw	12
	movwf	Bit
	movlw	4
	movwf	BitStart
	movlw	15
	
RxBaud	movwf	Temp		; Display Baud Rate.
	call	LUT_RS0

  ifndef DEBUG
        movlw   125
	movwf	Count
	call    Dlay5ms
	decfsz	Count
	 goto	$ - 2
  endif

	movf	BitStart,W	;  Setup the Values for the Interrupt
	movwf	BitCount
	movlw	8		;  Want to Read 8 Characters
	movwf	RRCount
	movlw	BufferStart     ;  Initialize the Indexes to the Buffer
	movwf	ShadowFSR
	movwf	Next
	movlw	intreg
	movwf	INTCON
	return
