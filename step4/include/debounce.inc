; ******************************************************************************************   
; This Macro waits for a PORT Pin to reach a set state for a set time before continuing. 
; TimeDelay = (((InitDlay - 1 ) * 256) * 7) / (Frequency / 4)                    
;                     or                        
;  InitDlay = ((TimeDelay * (Frequency / 4)) / (256 * 7)) + 1
;   This means that with an "InitDlay" value of 12, a 19.7 msec
;    debounce delay will be produced for a PICMicro running at 4 MHz.
Lo		equ 0	; Macro Assign.
InitDlay	equ 6   ; Macro Dly time
Bit0		equ 0   ; Bit assignments for Debounce Macro
Bit1		equ 1
Bit2		equ 2
Bit3		equ 3
Bit4		equ 4
Bit5		equ 5
Bit6		equ 6
Bit7		equ 7

Debounce macro HiLo, Port, Bit
	if HiLo == Lo
	 btfss Port,Bit	       ;  Is the Button Pressed?
	else
	 btfsc Port,Bit
	endif
	 goto $ - 1            ;   Yes - Wait for it to be Released
	 movlw InitDlay        ;  Wait for Release to be Debounced
	 movwf dly_Lo          ;  Have to Delay 20 msecs
	 movlw 0
	if HiLo == Lo
	 btfss Port,Bit        ;  If Button Pressed, Wait Again for it
	else
	 btfsc Port,Bit
	endif
	 goto $ - 6            ;   to be Released
	ifndef Debug           ;  Skip Small Loop if "Debug" Defined
 	 addlw 1               ;  Increment the Delay Count
 	 btfsc STATUS, Z       ;   Loop If Low Byte (w) Not Equal to Zero
	else
	 nop                   ;  Match the Number of Instructions
	 nop
	endif
	 decfsz dly_Lo,f
	goto $ - 5
 endm
