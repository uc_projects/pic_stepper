#include "oopdos.h"


class Motor{
private:
 int x,y,mnum;
 Box *frame,*step_btn;
 Databox *Mstep,*Mspeed,*Mphase;
 Xbox *Mfr;
 int motor_lut[4];
 unsigned int cphase;

public:
 Motor(){
  char *stat = "0001";      // startup phase
  char *phse = "Phase";     // Captions
  char *stp  = "Steps";
  char *spd  = "Speed";
  char *rf  = "FR";
  char *nl   = "001";       // default data
  int  nlut[] = {1,4,2,8};  // defaulte lut
  cphase = 0;          	    // count variable for lut(0,1,2,3)
  mnum = 0;                 // Motor Number
  for (x=0;x<=3;x++)	    // assign look up table values
   motor_lut[x] = nlut[x];
  x = 1;
  y = 5;
  frame = new Box(x,y,14,13,1);
  step_btn = new Box(x+4,y+9,1,1,0);
  Mphase = new Databox(x+3,y+4,phse,stat);
  Mstep = new Databox(x+3,y+5,stp,nl);
  Mspeed = new Databox(x+3,y+6,spd,nl);
  Mfr = new Xbox(x+6,y+7,0,rf);
  step_btn->box_capt(stp);
 }

 Motor(int x1,int y1,int num){
  char *stat = "0001";      // startup phase
  char *phse = "Phase";     // Captions
  char *stp  = "Steps";
  char *spd  = "Speed";
  char *rf  = "FR";
  char *nl   = "001";       // default data
  int  nlut[] = {16,64,32,128};  // defaulte lut
  cphase = 0;           // count variable for lut
  for (x=0;x<=3;x++)	// copy nlut to motor_lut
   motor_lut[x] = nlut[x];

  mnum = num;		// Set Motor number
  x = x1;
  y = y1;
  frame = new Box(x,y,14,13,1);
  step_btn = new Box(x+4,y+9,1,1,0);
  Mphase = new Databox(x+3,y+4,phse,stat);
  Mstep = new Databox(x+3,y+5,stp,nl);
  Mspeed = new Databox(x+3,y+6,spd,nl);
  Mfr = new Xbox(x+6,y+7,0,rf);
  step_btn->box_capt(stp);
 }
 void Motor_lut(const int nlut[]){   //set the lut values
  int i;
  for (i=0;i<=3;i++)
   motor_lut[i] = nlut[i];
 }
 int Motor_rf(){
  frame->showbox();                 //refresh the motor object
  gotoxy(x+4,y+2);
  cout << "Motor:" << mnum;
  Mphase->databox_rf();
  Mstep->databox_rf();
  Mspeed->databox_rf();
  Mfr->xbox_rf();
  step_btn->box_rf();
  return (motor_lut[cphase]);
 }

 int Motor_focus(int x1,int y1){
  if (((x1>=x)&&(x1<=x+14)) && ((y1>=y)&&(y1<=y+13))){  // if inbox frame
   Mstep->databox_click(x1,y1);                         // service member classes
   Mspeed->databox_click(x1,y1);
   Mfr->xbox_click(x1,y1);
   if (step_btn->box_click(x1,y1)){			// 1 = Step motor
    return (1);
   }
  }
  return (0);
 }

 int Motor_direction(){
  return (Mfr->xbox_tf());
 }
 int Motor_speed(){
  return (Mspeed->databox_getint_txt());
 }
 int Motor_steps(){
  return (Mstep->databox_getint_txt());
 }

 int Motor_Step(int steps, int dly, int latch, int direction, int data){

  int i,aint;
  int msb;
  char *binstr = "0000";

  for (;steps>0;steps--){
   if (direction)
    cphase++;					// inc\dec current phase
   else
    cphase--;

   if (cphase > 4)
    cphase = 3;
   if (cphase == 4)				// if overflow reset
    cphase = 0;

   aint = motor_lut[cphase];			// store new phase to a int

   if (aint > 8)	                	// if that int is more than 8
    aint /= 16;					// then divide the nybble by 16

//   gotoxy(x+5,y+12);				// Print Currrent phase
//   cout << aint << " ";		                // as an int

   msb = 8;
   for (i=0;i<4;i++){				// convert the int 2
    if ((aint & msb) > 0)			//  a binary string.
     binstr[i] = '1';
    else
     binstr[i] = '0';
    msb /= 2;
   }
   Mphase->databox_text(binstr);		// print the string on the screen.

   aint = data + motor_lut[cphase];             // add the nybbles
   gotoxy(40,3);				// print the data
   cout << "m0 + m1: " << aint << "    ";
   Shift_Out(aint);				// shift the data out

   if (latch == 0)
    LE0_Strobe();				// and latch the data.
   else
    LE1_Strobe();
   delay(dly);					// motor hold time
  }
  return (motor_lut[cphase]);
 }

// Spin the Motor revol times                   //
 int Motor_Spin(int revol, int dly, int latch, int data){
  int count;
  int datao;
  for (;revol>0;revol--){			// step revol * 4
   for (count=0;count<5;count++){
    cphase++;					// increment current phase
    if (cphase == 4) 				// if overflow reset
     cphase = 0;
//    gotoxy(x+5,y+12);				//print the current phase
//    cout << motor_lut[cphase] << " ";		// as an int

    datao = data + motor_lut[cphase];           // add the nybbles
    gotoxy(40,3);				// print the data
    cout << "m0 + m1: " << datao << "    ";
    Shift_Out(data);				// shift the data out

    if (latch == 0)
     LE0_Strobe();				//and latch the data.
    else
     LE1_Strobe();
    delay(dly);					// motor hold time
   }
  }
  return (motor_lut[cphase]);
 }

};
