//#include <time.h>
#include <conio.h>

//#include <stdlib.h>
#include <iostream.h>
#include <stdio.h>
#include <string.h>

class Box{
protected:
 int x,y;			// top left corner
 int w,h;      			// width & height
 int linetype; 			// 1=double, 0=single
 char caption[10];

 void hline(char c){
  for(int i=x+1;i<x+w;i++){
   gotoxy(i,y);
   cout << c;
   gotoxy(i,y+h);
   cout << c;
  }
 }
 void vline(char c){
  for(int i=y+1;i<y+h;i++){
   gotoxy(x,i);
   cout << c;
   gotoxy(x+w,i);
   cout << c;
  }
 }
 void drawcorners(){
  char ltc,lbc,rtc,rbc;

  if (linetype == 1){
   ltc = 201;
   lbc = 200;
   rtc = 187;
   rbc = 188;
  }
  else{
   ltc = 218;
   lbc = 192;
   rtc = 191;
   rbc = 217;
  }

   gotoxy(x,y);
   cout << ltc;
   gotoxy(x,y+h);
   cout << lbc;
   gotoxy(x+w,y);
   cout << rtc;
   gotoxy(x+w,y+h);
   cout << rbc;
 }
 void erasecorners(){
   gotoxy(x,y);
   cout << ' ';
   gotoxy(x,y+h);
   cout << ' ';
   gotoxy(x+w,y);
   cout << ' ';
   gotoxy(x+w,y+h);
   cout << ' ';
 }

public:
 Box(){
  char *dcapt = "";
  strcpy(caption, dcapt);
  x = 1;
  y = 1;
  w = 1;
  h = 1;
  linetype = 1;
 }
 Box(int x1,int y1,int w1,int h1,int lt){
  char *dcapt = "";
  strcpy(caption, dcapt);
  if ((((x1+w1+1)<=80) && x1>=1) && ((y1+h1+1)<=26 && y1>=1) && (lt==1 || lt==0)){
   x = x1;
   y = y1;
   w = w1;
   h = h1;
   linetype = lt;
  }
  else{
   x = 1;
   y = 1;
   w = 1;
   h = 1;
   linetype = 1;
  }
 }
 void showbox(){
  char hr,vr;
  if (linetype == 1){
   hr = 205;
   vr = 186;
  }
  else{
   hr = 196;
   vr = 179;
  }
  gotoxy(x,y);
  drawcorners();
  hline(hr);
  vline(vr);
 }
 void hidebox(){
  char m = ' ';
  gotoxy(x,y);
  erasecorners();
  hline(m);
  vline(m);
 }
 void movebox(int x1, int y1){
  if ((((x1+w+2)<=80) && x1>=1) && ((y1+h+2<=26) && y1>=1)){
   hidebox();
   x = x1;
   y = y1;
   showbox();
  }
 }
 void moveboxr(){
  hidebox();
  if ((x+w+2)<=80)
   x++;
  showbox();
 }
 void moveboxl(){
  hidebox();
  if (x>=2)
  x--;
  showbox();
 }
 void moveboxu(){
  hidebox();
  if (y>=2)
   y--;
  showbox();
 }
 void moveboxd(){
  hidebox();
  if ((y+h+2)<=26)
   y++;
  showbox();
 }
 int getx(){
  return (x);
 }
 int gety(){
  return (y);
 }
 int geth(){
  return (h);
 }
 int getw(){
  return (w);
 }
 void box_rf(){
  showbox();
  gotoxy(x+1,y+1);
  cout << caption;
 }
 void box_capt(const char *capt){
  strcpy(caption, capt);
  w = strlen(capt) + 1;
  h = 2;
 }
 int box_click(int x1, int y1){
  if (((x1>=x)&&(x1<=x+w))&&((y1>=y)&&(y1<=y+h+2))){
   delay(10);
   hidebox();
   delay(70);
   showbox();
   return (1);
  }
  return (0);
 }
};

//x,y    _     7 Segment Display object
//x,y-1 |_|
//x,y-2 |_|
class Seg7{
protected:
 int x,y;

public:
 Seg7(){
  x = 1;
  y = 1;
 }

 Seg7(int x1,int y1){
  x = x1;
  y = y1;
 }

 void seg7_rf(int d){
  if (!(d <= 9 && d>=0))
   d = 10;

   gotoxy(x,y);
   switch (d){
   case 0:
    cout << " _";
    gotoxy(x,y+1);
    cout << "| |";
    gotoxy(x,y+2);
    cout << "|_|";
    break;
   case 1:
    cout << "   ";
    gotoxy(x,y+1);
    cout << "  |";
    gotoxy(x,y+2);
    cout << "  |";
    break;
   case 2:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << " _|";
    gotoxy(x,y+2);
    cout << "|_ ";
    break;
   case 3:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << " _|";
    gotoxy(x,y+2);
    cout << " _|";
    break;
   case 4:
    cout << "   ";
    gotoxy(x,y+1);
    cout << "|_|";
    gotoxy(x,y+2);
    cout << "  |";
    break;
   case 5:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "|_ ";
    gotoxy(x,y+2);
    cout << " _|";
    break;
   case 6:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "|_ ";
    gotoxy(x,y+2);
    cout << "|_|";
    break;
   case 7:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "  |";
    gotoxy(x,y+2);
    cout << "  |";
    break;
   case 8:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "|_|";
    gotoxy(x,y+2);
    cout << "|_|";
    break;
   case 9:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "|_|";
    gotoxy(x,y+2);
    cout << " _|";
    break;
   case 10:
    cout << "   ";
    gotoxy(x,y+1);
    cout << "   ";
    gotoxy(x,y+2);
    cout << "   ";
  }
  textcolor(GREEN);
 }
};

class Databox{
protected:
 char caption[10];
 char text[5];
 int  x,y;

public:
 Databox(){
  char *dcapt = "caption";
  char *dtext = "text";
  strcpy(caption, dcapt);
  strcpy(text, dtext);
  x = 1;
  y = 1;
 }
 Databox(int x1,int y1,const char *capt,const char *txt){
  x = x1;
  y = y1;
  strcpy(caption, capt);
  strcpy(text, txt);
 }
 void databox_rf(){
  gotoxy(x,y);
  cout << caption << ":" << text;
 }
 void databox_text(const char *txt){
  strcpy(text, txt);
  databox_rf();
 }
 void databox_click(int x1,int y1){
  int db_temp = 0;
  char c = ' ';
  char *nll = "";

  if ( (x1>=(strlen(caption)+1+x)) &&
  (x1<=(strlen(caption) + strlen(text)+x)) && (y1==y)){
   strcpy(text, nll);  //clear the text
   databox_rf();

   for (;db_temp<3;db_temp++){
    while (!(c>=48 && c<=57)){  //loop until a # char is pressed
    c = getch();
    }
    cout << c;
    text[db_temp] = c;
    c = ' ';
   }
  }
 }
 int databox_getint_txt(){
  int a;
  a = (text[0]-48);
  a *=10;
  a += (text[1]-48);
  a *=10;
  a += (text[2]-48);
  return (a);
 }
};


class Xbox{
private:
 int x,y,tf;
 char caption[10];


 void xbox_toggle(){
  if(tf == 1)
   tf = 0;
  else
   tf = 1;
  xbox_rf();
 }

public:
 Xbox(){
  x = 1;
  y = 1;
  tf = 0;
 }

 Xbox(int x1,int y1,int tf1,const char *capt){
  x = x1;
  y = y1;
  tf = tf1;
  strcpy(caption, capt);
 }
 void xbox_rf(){
  gotoxy(x,y);
  if (tf == 1)
   cout << caption << ":[X]";
  else
   cout << caption << ":[ ]";
 }
 void xbox_click(int x1,int y1){
  if (x1==(strlen(caption)+2+x)&& y1==y)
   xbox_toggle();
 }
 int xbox_tf(){
  return (tf);
 }
};

