;*****************************************************************
;* PortB                       		      V+
;* 5 Coulmn 2 -------------------+-------/\/---|  internal pullup
;* 4 Coulmn 1 -------------+-----|-------/\/---|   resistors
;* 3 Coulmn 0 -------+-----|-----|-------/\/---|
;*                   |     |     |
;* 74164             |     |     |
;* 7  Row 3 --|<---[3,5]-[3,6]-[2,5]
;* 6  Row 2 --|<---[1,4]-[1,5]-[0,4]
;* 5  Row 1 --|<---[1,3]-[0,1]-[1,1]
;           1N914

;*****************************************************************
TestKey	movlw	b'00011011'	; Test 4 a key press!
	call	SOut

	movf	PORTB,W		; Get Rows
	andlw	b'00111000'	;  = 112

; IF W < 112 then a key is pressed.
	sublw	b'00111000'	; 112 - W
	btfsc	STATUS,Z	; If W = 112 then no key pressed.
	 retlw	0

	btfss	STATUS,C
	 retlw	0		; W can't be > 112!!!
	retlw	1		; Key Pressed

;*****************************************************************
keypad	movlw	b'11011100'	; Test 4 a key press!
	call	SOut

	movlw	3		; 3 columns
	movwf	DCount
	movlw	1
	movwf	DTemp

Row0	btfsc	PORTB,3
	 goto	Row1
	Debounce 0, PORTB, Bit3
	movf	DTemp,W
	return

Row1	incf	DTemp,F
	btfsc	PORTB,4
	 goto	Row2
	Debounce 0, PORTB, Bit4
	bsf	INDF,1
	movf	DTemp,W
	return

Row2	incf	DTemp,F
	btfsc	PORTB,5
	 goto	Rlp
	Debounce 0, PORTB, Bit5
	movf	DTemp,W
	return

Rlp	incf	DTemp,F
	bsf	PORTB,Clk	; rlf 74164,F
	bcf	PORTB,Clk
	call	Dlay160
	decfsz	DCount,F
	 goto	Row0
	retlw	0
