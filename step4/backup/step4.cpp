#include "dos.h"        // Library Headers
#include "conio.h"
#include "string.h"
#include "iostream.h"

//#include "timer.h"
#include "lpt.h"
#include "oopdos.h"
#include "mouse.cpp"
#include "cgc.cpp"


struct text_info t_info; // Structure to hold mouse Info

//----------------------------------------------------------------------------//
//                               Program initalization start                  //
//----------------------------------------------------------------------------//
int main(){

//-- application variables declaration section -------------------------------//
  int x = 1;		//text cursor coordinates
  int y = 5;
  int x1,y1 = 0;		//mouse cursor coordinates
  unsigned int lb,rb;
  int sys = 0;
  int nlut[] = {16,64,32,128};
  int nlut2[] = {1,2,4,8};
  int m0_phase, m1_phase; //m0_phase + m1_phase = SIPO Data on latch0
  int m2_phase;
  char *menu1 = "Quit";

//-- application objects declaration section ---------------------------------//
  clrscr();     // clear the screen to display declaration info

  Box menu(1,1,78,3,0);
  Box Quit(1,1,7,3,0);
  Quit.box_capt(menu1);

  Motor m0;		// Motor contains(2databox,1Caption,1Button,1frame)
  Motor m1(16,5,1);

  m1.Motor_lut(nlut);	// set the lut values
  Motor m2(31,5,2);
  m2.Motor_lut(nlut2);	// set the lut values

//  Motor m3(46,5,3);
//  m3.Motor_lut(nlut);	// set the lut values
//    getch();   // wait for user

//----------------------------------------------------------------------------//
//          init the lpt port                                                 //
//----------------------------------------------------------------------------//
  x = init();          // init sends 0 if LPT ports could not be found
  if (!x){
   gotoxy(2,2);
   cout << "No LPT port Detected, Program Halted";
   return (0);
  }
  else{
   gotoxy(3,3);       // Print the parallel port info
   LPT_info();
   cout << endl;
//   Print_Port();      // lpt 1
//   cout << endl;

   clrprt();
//   test_lpt();
//   test_ShiftOut();    // test shift out routine
   clrscr();
   }


//----------------------------------------------------------------------------//
//         initalize the mouse & screen size                                  //
//----------------------------------------------------------------------------//
  if(mouse.Exists()){   		// check for mouse
    mouse.InstallHandler(0xFF);		// install event handler
    mouse.Enable();			// enable mouse

/*    textmode(C80);                      // 80x25 color text mode
    mouse.xLimit(16, 623);              // set x limit
    mouse.yLimit(0, 199);               // set y limit
*/
    textmode(C4350);                  // 43/50 line mode
      _setcursortype(_NOCURSOR);        // turn the hardware cursor off
    mouse.xLimit(0, 639);             // set x limit
    gettextinfo(&t_info);
    if(t_info.screenheight == 43)
      mouse.yLimit(0, 349);           // set y limit to EGA 43 line
    else
      mouse.yLimit(0, 399);           // set y limit to VGA 50 line

/*    gotoxy(2,2);                      // Print the type of mouse found
    if((int)mouse.Info.type == 1) cout << " Bus mouse found ";
    if((int)mouse.Info.type == 2) cout << " Serial mouse found ";
    if((int)mouse.Info.type == 3) cout << " InPort mouse found ";
    if((int)mouse.Info.type == 4) cout << " PS/2 mouse found ";
    if((int)mouse.Info.type == 5) cout << " HP mouse found ";
*/    cout << "Software version is " <<(int)mouse.Info.majorvers<<"."<<(int)mouse.Info.minorvers;
  }
  else{
   gotoxy(2,2);
   cout << "No mouse Detected, Program Halted";
   return (0);
  }
// getch();
// clrscr();

//--------------------------------------------------
//               test timer
//--------------------------------------------------
//  Print_Time_Regs();
//  getch();
//  clrscr();

//----------------------------------------------------------------------------//
//                               Program initalization end                    //
//----------------------------------------------------------------------------//

  mouse.Show();     		 //show App framework
  menu.showbox();
  Quit.showbox();
  Quit.box_rf();

  m0_phase = m0.Motor_rf();      //Draw objects & init variables
  m1_phase = m1.Motor_rf();
  m2_phase = m2.Motor_rf();
//  m3.Motor_rf();

//  m0_phase = m0.Motor_Spin(25,0,0);       // test the motors
//  m1_phase = m1.Motor_Spin(25,0,m0_phase); // pass current phase to next motor test!
  m2_phase = m2.Motor_Spin(90,1,0);

  m0_phase = m0.Motor_rf();      //Draw objects & init variables
  m1_phase = m1.Motor_rf();
  m2_phase = m2.Motor_rf();
//  m3.Motor_rf();


  for(;!kbhit();){
    mouse.GetEvent();            // Disp Mouse Data
    x = (mouse.xPos()/8)+1;
    y = (mouse.yPos()/8)+1;

    if(mouse.LB_Dn())		// check for left button down
      lb = 1;
    else
      lb = 0;
    if(mouse.RB_Dn())		// check for right button down
      rb = 1;
    else
      rb = 0;

   if ((x != x1 || y != y1) || (lb == 1 || rb == 1)){ // Mouse moved ?
     gotoxy(57,2);
     cout <<"X:"<<x <<" Y:"<<y <<" LB:"<<lb << " RB:"<<rb<< "  ";
     if (lb == 1){                        // button pressed?
      mouse.Hide();

      lb = m0.Motor_focus(x,y);
      if (lb){
       m0_phase = m0.Motor_Step(0,m1_phase);  //step motot x1 times
      }

      lb = m1.Motor_focus(x,y);
      if (lb){
       m1_phase = m1.Motor_Step(0,m0_phase);  //step motor x1 times
      }

      lb = m2.Motor_focus(x,y);
      if (lb){
       m2_phase = m2.Motor_Step(1,0);  //step motor x1 times
      }

      if ((x >= 1 && x <= 7) && (y >= 1 && y <= 3)){
       Quit.box_click(x,y);
       sys = 1;
      }

      mouse.Show();
      do{			   // Debounce Mouse buttons
       mouse.GetEvent();
       if(mouse.LB_Dn())           // check for left button down
	lb = 1;
       else
	lb = 0;
       if(mouse.RB_Dn())           // check for Right button down
	rb = 1;
       else
	rb = 0;
      }while (lb==1||rb==1);
     }
     y1 = y;
     x1 = x;
   }
  if (sys)
   break;
  }
  clrprt();                       // Clear the Latches
  mouse.Disable();                // disable the mouse
  clrscr();       		  // clear the screen
  return (1);
}
