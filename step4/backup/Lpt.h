/* Global variables */
int port;                 /* Currently scanned port  */
int port_positions[4];    /* List of all ports       */
int port_number;          /* Number of present ports */

#define MY_SHORT

/*---------------------------------------------------------------------*/
/*             initalize the objects port variables                    */
/*---------------------------------------------------------------------*/
void getports (void){
// unsigned int address;     /* Address of Port */
 int j;
 unsigned MY_SHORT int far *ptraddr;

 ptraddr = (unsigned MY_SHORT int far *)0x00000408;
 port_number = 0;

 for (j = 0; j < 3; j++){
  port_positions[j] = *(ptraddr++);
  if (port_positions[j])
   port_number++;
 }
}

/*---------------------------------------------------------------------*/
/*             initalize the objects port variables                    */
/*---------------------------------------------------------------------*/
int init(){
 getports();
 if (!port_number)
  return(0);

 port = port_positions[0];   /* Set default port to LPT1 */
 return (1);
}

/*---------------------------------------------------------------------*/
/*             input data from port                                    */
/*---------------------------------------------------------------------*/
void Print_Port(){
  int base0,       /* buffer for port + 0 */
      base1,       /* buffer for port + 1 */
      base2;       /* buffer for port + 2 */

  base0 = inportb(port);   // read the port
  base1 = inportb(port+1);
  base2 = inportb(port+2);

  cout << "  base0: " << base0 << endl; // print the data
  cout << "  base1: " << base1 << endl;
  cout << "  base2: " << base2 << endl;
}

/*---------------------------------------------------------------------*/
/*  Print all usable lpt ports detected                                */
/*---------------------------------------------------------------------*/
void LPT_info(){
 if (port_number){
 // print last port
  cout << "default lpt port: " << port_positions[port_number - 1];
 }
 else{
  cout << "No Parallel Ports detected!!!";
 }
}

/*Step4 routines ----------------------------------------------------*/
/*       port              port + 2
  [L2 L0 L1 A0 A1 A2 MR] |   D
   1  2  4  8  16 32 64      1    = bit weights

[  1  1  1  0  0  1  0       0    =  output        ] Reset
[  0  0  0  1  1  0  1 = 88  0    =  port output   ]

[  0  0  0  0  0  1  1       0    =  output        ] address
[  1  1  1  1  1  0  0 = 31  0    =  port output   ] mode

[  1  0  0  0  0  1  1       0    =  output        ] memory mode
[  0  1  1  1  1  0  0 = 30  0    =  port output   ]

[  1  1  0  0  0  1  1       0    =  output        ] latch 0
[  0  0  1  1  1  0  0 = 28  0    =  port output   ] mode

[  1  0  1  0  1  1  1       0    =  output        ] latch 1
[  0  1  0  1  0  0  0 = 10  0    =  port output   ] mode

   note that all address lines point to Q7                           */
/*-------------------------------------------------------------------*/

void clrprt(){
 outportb(port + 2,0);   // D=0
 outportb(port,88);      // reset mode
 delay(1);		 // wait for 1ms for data valid!!!

 outportb(port,30);      // memory mode
}

void LE0_Strobe(){      /* Latch Enable Motors 0 & 1 */
 int pdata;

 pdata = inportb(port); // get data from port
 pdata -= 2;            // latch 0 mode
 outportb(port,pdata);
// delay(1);
 pdata += 2;
 outportb(port,pdata);  // previous mode
}

void LE1_Strobe(){      /* Latch Enable Motors 2 & 3 */
int pdata;

 pdata = inportb(port); // get data from port
 pdata -= 4;            // latch 1 mode
 outportb(port,pdata);
// delay(1);
 pdata += 4;
 outportb(port,pdata);  // previous mode
}

void LPData0(){
  outportb(port+2,0);
//  delay(1);
}

void LPData1(){
  outportb(port+2,1);
//  delay(1);
}



void Shift_Out(int i){

 outportb(port,31);	   // address mode + address
 if ((i & 128) == 128){    // set data bit
  LPData1();
//  cout << "1";
 }
 else{
  LPData0();
//  cout << "0";
 }
// delay(1);               // wait for data valid

 outportb(port,23);
 if ((i & 64) == 64){
  LPData1();
//  cout << "1";
 }
 else{
  LPData0();
//  cout << "0";
 }
// delay(1);

 outportb(port,15);
 if ((i & 32) == 32){
  LPData1();
//  cout << "1";
 }
 else{
  LPData0();
//  cout << "0";
 }
// delay(1);

 outportb(port,7);
 if ((i & 16) == 16){
  LPData1();
//  cout << "1";
 }
 else{
  LPData0();
//  cout << "0";
 }
// delay(1);

 outportb(port,39);
 if ((i & 8) == 8){
  LPData1();
//  cout << "1";
 }
 else{
  LPData0();
//  cout << "0";
 }
// delay(1);

 outportb(port,47);
 if ((i & 4) == 4){
  LPData1();
//  cout << "1";
 }
 else{
  LPData0();
//  cout << "0";
 }
// delay(1);

 outportb(port,55);
 if ((i & 2) == 2){
  LPData1();
//  cout << "1";
 }
 else{
  LPData0();
//  cout << "0";
 }
// delay(1);

 outportb(port,63);
 if ((i & 1) == 1){
  LPData1();
//  cout << "1";
 }
 else{
  LPData0();
//  cout << "0";
 }
// delay(1);


// reload MSB because of some bug?
 outportb(port,31);	    //load the address & mode
 if ((i & 128) == 128){     // set data bit
  LPData1();
 }
 else{
  LPData0();
 }
// delay(1);               // wait for data valid

 outportb(port,30);      // memory mode
 outportb(port+2,0);     // clear the data bit
}


void test_ShiftOut(){
 char chin[] = "00000000";
 char vbit = 'y';
 int i;

 cout << endl << " Byte Output routine" << endl << endl;

while (vbit == 'y'){
  cout << " enter 8bit binary # to output ";
  cin >> chin;
  i = 0;
  if (chin[0] == '1')
   i = 128;
  if (chin[1] == '1')
   i += 64;
  if (chin[2] == '1')
   i += 32;
  if (chin[3] == '1')
   i += 16;
  if (chin[4] == '1')
   i += 8;
  if (chin[5] == '1')
   i += 4;
  if (chin[6] == '1')
   i += 2;
  if (chin[7] == '1')
   i += 1;
  cout << "bus latch = " << i;

  Shift_Out(i);
  cout << "        loop ? y,n ";
  cin >> vbit;
 }
}

/*---------------------------------------------------------------------*/
/*  test 74259 latch reg                                               */
/*---------------------------------------------------------------------*/
void test_lpt(){
//               inverted          double inverted
//dataline  [d0 d1 d2 d3 d4 d5 d6]    strobe
//pin        D  L0 L1 A0 A1 A2 MR       L2

// int i = 128;
// int c = 8;
 char vbit = 'y';


// cout << " test connector " << endl;
// for (;c > 0;c--){
//  cout << port << " data = " << i << endl;
//  outportb(port,i);
//  getch();
//  i /= 2;
// }

while (vbit == 'y'){
 clrscr();
 outportb(port+2,0);       // clear the data bit
 cout << endl << " test I\O ;|" << endl << endl;

 cout << " enter a logic level for bit A7 ";
 cin >> vbit;
 outportb(port,31);
 if (vbit == '1')
  LPData1();
 else
  LPData0();
// cout << endl << "port " << port << " = " << i << endl;
// delay(1);

 cout << " enter a logic level for bit A6 ";
 cin >> vbit;
 outportb(port,23);
 if (vbit == '1')
  LPData1();
 else
  LPData0();
// cout << endl << "port " << port << " = " << i << endl;
// delay(1);

 cout << " enter a logic level for bit A5 ";
 cin >> vbit;
 outportb(port,15);
 if (vbit == '1')
  LPData1();
 else
  LPData0();
// cout << endl << "port " << port << " = " << i << endl;
// delay(1);

// Q4=Q7,Q5=Q6, Q6=Q5, Q7=Q4  Qinput latch=Qoutput latch
 cout << " enter a logic level for bit A4 ";
 cin >> vbit;
 outportb(port,7);
 if (vbit == '1')
  LPData1();
 else
  LPData0();
// cout << endl << "port " << port << " = " << i << endl;
// delay(1);

 cout << " enter a logic level for bit A3 ";
 cin >> vbit;
 outportb(port,39);
 if (vbit == '1')
  LPData1();
 else
  LPData0();
// cout << endl << "port " << port << " = " << i << endl;
// delay(1);

 cout << " enter a logic level for bit A2 ";
 cin >> vbit;
 outportb(port,47);
 if (vbit == '1')
  LPData1();
 else
  LPData0();
// cout << endl << "port " << port << " = " << i << endl;
// delay(1);

 cout << " enter a logic level for bit A1 ";
 cin >> vbit;
 outportb(port,55);
 if (vbit == '1')
  LPData1();
 else
  LPData0();
// cout << endl << "port " << port << " = " << i << endl;
// delay(1);

 cout << " enter a logic level for bit A0 ";
 cin >> vbit;
 outportb(port,63);        // output the address
 if (vbit == '1')          // output the data bit
  LPData1();
 else
  LPData0();
// cout << endl << "port " << port << " = " << i << endl;
// delay(1);

 cout << " loop ? y,n ";
 cin >> vbit;
}

 LE0_Strobe();
 getch();
}
