class Box{
protected:
 int x,y;			// top left corner
 int w,h;      			// width & height
 int linetype; 			// 1=double, 0=single
 char caption[10];

 void hline(char c){
  for(int i=x+1;i<x+w;i++){
   gotoxy(i,y);
   cout << c;
   gotoxy(i,y+h);
   cout << c;
  }
 }
 void vline(char c){
  for(int i=y+1;i<y+h;i++){
   gotoxy(x,i);
   cout << c;
   gotoxy(x+w,i);
   cout << c;
  }
 }
 void drawcorners(){
  char ltc,lbc,rtc,rbc;

  if (linetype == 1){
   ltc = 201;
   lbc = 200;
   rtc = 187;
   rbc = 188;
  }
  else{
   ltc = 218;
   lbc = 192;
   rtc = 191;
   rbc = 217;
  }

   gotoxy(x,y);
   cout << ltc;
   gotoxy(x,y+h);
   cout << lbc;
   gotoxy(x+w,y);
   cout << rtc;
   gotoxy(x+w,y+h);
   cout << rbc;
 }
 void erasecorners(){
   gotoxy(x,y);
   cout << ' ';
   gotoxy(x,y+h);
   cout << ' ';
   gotoxy(x+w,y);
   cout << ' ';
   gotoxy(x+w,y+h);
   cout << ' ';
 }

public:
 Box(){
  char *dcapt = "";
  strcpy(caption, dcapt);
  x = 1;
  y = 1;
  w = 1;
  h = 1;
  linetype = 1;
 }
 Box(int x1,int y1,int w1,int h1,int lt){
  char *dcapt = "";
  strcpy(caption, dcapt);
  if ((((x1+w1+1)<=80) && x1>=1) && ((y1+h1+1)<=26 && y1>=1) && (lt==1 || lt==0)){
   x = x1;
   y = y1;
   w = w1;
   h = h1;
   linetype = lt;
  }
  else{
   x = 1;
   y = 1;
   w = 1;
   h = 1;
   linetype = 1;
  }
 }
 void showbox(){
  char hr,vr;
  if (linetype == 1){
   hr = 205;
   vr = 186;
  }
  else{
   hr = 196;
   vr = 179;
  }
  gotoxy(x,y);
  drawcorners();
  hline(hr);
  vline(vr);
 }
 void hidebox(){
  char m = ' ';
  gotoxy(x,y);
  erasecorners();
  hline(m);
  vline(m);
 }
 void movebox(int x1, int y1){
  if ((((x1+w+2)<=80) && x1>=1) && ((y1+h+2<=26) && y1>=1)){
   hidebox();
   x = x1;
   y = y1;
   showbox();
  }
 }
 void moveboxr(){
  hidebox();
  if ((x+w+2)<=80)
   x++;
  showbox();
 }
 void moveboxl(){
  hidebox();
  if (x>=2)
  x--;
  showbox();
 }
 void moveboxu(){
  hidebox();
  if (y>=2)
   y--;
  showbox();
 }
 void moveboxd(){
  hidebox();
  if ((y+h+2)<=26)
   y++;
  showbox();
 }
 int getx(){
  return (x);
 }
 int gety(){
  return (y);
 }
 int geth(){
  return (h);
 }
 int getw(){
  return (w);
 }
 void box_rf(){
  showbox();
  gotoxy(x+1,y+1);
  cout << caption;
 }
 void box_capt(const char *capt){
  strcpy(caption, capt);
  w = strlen(capt) + 1;
  h = 2;
 }
 int box_click(int x1, int y1){
  if (((x1>=x)&&(x1<=x+w))&&((y1>=y)&&(y1<=y+h+2))){
   delay(10);
   hidebox();
   delay(70);
   showbox();
   return (1);
  }
  return (0);
 }
};

//x,y    _     7 Segment Display object
//x,y-1 |_|
//x,y-2 |_|
class Seg7{
protected:
 int x,y;

public:
 Seg7(){
  x = 1;
  y = 1;
 }

 Seg7(int x1,int y1){
  x = x1;
  y = y1;
 }

 void seg7_rf(int d){
  if (!(d <= 9 && d>=0))
   d = 10;

   gotoxy(x,y);
   switch (d){
   case 0:
    cout << " _";
    gotoxy(x,y+1);
    cout << "| |";
    gotoxy(x,y+2);
    cout << "|_|";
    break;
   case 1:
    cout << "   ";
    gotoxy(x,y+1);
    cout << "  |";
    gotoxy(x,y+2);
    cout << "  |";
    break;
   case 2:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << " _|";
    gotoxy(x,y+2);
    cout << "|_ ";
    break;
   case 3:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << " _|";
    gotoxy(x,y+2);
    cout << " _|";
    break;
   case 4:
    cout << "   ";
    gotoxy(x,y+1);
    cout << "|_|";
    gotoxy(x,y+2);
    cout << "  |";
    break;
   case 5:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "|_ ";
    gotoxy(x,y+2);
    cout << " _|";
    break;
   case 6:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "|_ ";
    gotoxy(x,y+2);
    cout << "|_|";
    break;
   case 7:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "  |";
    gotoxy(x,y+2);
    cout << "  |";
    break;
   case 8:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "|_|";
    gotoxy(x,y+2);
    cout << "|_|";
    break;
   case 9:
    cout << " _ ";
    gotoxy(x,y+1);
    cout << "|_|";
    gotoxy(x,y+2);
    cout << " _|";
    break;
   case 10:
    cout << "   ";
    gotoxy(x,y+1);
    cout << "   ";
    gotoxy(x,y+2);
    cout << "   ";
  }
  textcolor(GREEN);
 }
};

class Databox{
protected:
 char caption[10];
 char text[5];
 int  x,y;

public:
 Databox(){
  char *dcapt = "caption";
  char *dtext = "text";
  strcpy(caption, dcapt);
  strcpy(text, dtext);
  x = 1;
  y = 1;
 }
 Databox(int x1,int y1,const char *capt,const char *txt){
  x = x1;
  y = y1;
  strcpy(caption, capt);
  strcpy(text, txt);
 }
 void databox_rf(){
  gotoxy(x,y);
  cout << caption << ":" << text;
 }
 void databox_text(const char *txt){
  strcpy(text, txt);
  databox_rf();
 }
 void databox_click(int x1,int y1){
  int db_temp = 0;
  char c = ' ';
  char *nll = "";

  if ( (x1>=(strlen(caption)+1+x)) &&
  (x1<=(strlen(caption) + strlen(text)+x)) && (y1==y)){
   strcpy(text, nll);  //clear the text
   databox_rf();

   for (;db_temp<3;db_temp++){
    while (!(c>=48 && c<=57)){  //loop until a # char is pressed
    c = getch();
    }
    cout << c;
    text[db_temp] = c;
    c = ' ';
   }
  }
 }
 int databox_getint_txt(){
  int a;
  a = (text[0]-48);
  a *=10;
  a += (text[1]-48);
  a *=10;
  a += (text[2]-48);
  return (a);
 }
};


class Xbox{
private:
 int x,y,tf;
 char caption[10];


 void xbox_toggle(){
  if(tf == 1)
   tf = 0;
  else
   tf = 1;
  xbox_rf();
 }

public:
 Xbox(){
  x = 1;
  y = 1;
  tf = 0;
 }

 Xbox(int x1,int y1,int tf1,const char *capt){
  x = x1;
  y = y1;
  tf = tf1;
  strcpy(caption, capt);
 }
 void xbox_rf(){
  gotoxy(x,y);
  if (tf == 1)
   cout << caption << ":[X]";
  else
   cout << caption << ":[ ]";
 }
 void xbox_click(int x1,int y1){
  if (x1==(strlen(caption)+2+x)&& y1==y)
   xbox_toggle();
 }
 int xbox_tf(){
  return (tf);
 }
};


/*-----------------------------------------------------------------------*/
class Motor{
private:
 int x,y,mnum;
 int spr;		    // #steps per revolution
 Box *frame,*step_btn;
 Databox *Mstep,*Mspeed,*Mphase;
 Xbox *Mfr;
 int motor_lut[4];
 unsigned int cphase;

public:
 Motor(){
  char *stat = "0001";      // startup phase
  char *phse = "Phase";     // Captions
  char *stp  = "Steps";
  char *spd  = "Speed";
  char *rf  = "FR";
  char *nl   = "003";       // default data
  int  nlut[] = {1,4,2,8};  // default lut
  cphase = 0;          	    // count variable for lut(0,1,2,3)
  mnum = 0;                 // Motor Number
  for (x=0;x<=3;x++)	    // assign look up table values
   motor_lut[x] = nlut[x];
  x = 1;
  y = 5;
  spr = 200;		    // 1.8 degree per step default
  frame = new Box(x,y,14,13,1);
  step_btn = new Box(x+4,y+9,1,1,0);
  Mphase = new Databox(x+3,y+4,phse,stat);
  Mstep = new Databox(x+3,y+5,stp,nl);
  Mspeed = new Databox(x+3,y+6,spd,nl);
  Mfr = new Xbox(x+6,y+7,0,rf);
  step_btn->box_capt(stp);
 }

 Motor(int x1,int y1,int num){
  char *stat = "0001";      // startup phase
  char *phse = "Phase";     // Captions
  char *stp  = "Steps";
  char *spd  = "Speed";
  char *rf  = "FR";
  char *nl   = "003";       // default data
  int  nlut[] = {16,64,32,128};  // defaulte lut
  cphase = 0;           // count variable for lut
  for (x=0;x<=3;x++)	// copy nlut to motor_lut
   motor_lut[x] = nlut[x];

  mnum = num;		// Set Motor number
  x = x1;
  y = y1;
  spr = 200;		    // 1.8 degree per step default
  frame = new Box(x,y,14,13,1);
  step_btn = new Box(x+4,y+9,1,1,0);
  Mphase = new Databox(x+3,y+4,phse,stat);
  Mstep = new Databox(x+3,y+5,stp,nl);
  Mspeed = new Databox(x+3,y+6,spd,nl);
  Mfr = new Xbox(x+6,y+7,1,rf);
  step_btn->box_capt(stp);
 }
 void Motor_lut(const int nlut[]){   //set the lut values
  int i;
  for (i=0;i<=3;i++)
   motor_lut[i] = nlut[i];
 }
 int Motor_rf(){
  frame->showbox();                 //refresh the motor object
  gotoxy(x+4,y+2);
  cout << "Motor:" << mnum;
  Mphase->databox_rf();
  Mstep->databox_rf();
  Mspeed->databox_rf();
  Mfr->xbox_rf();
  step_btn->box_rf();
  return (motor_lut[cphase]);
 }

 int Motor_focus(int x1,int y1){
  if (((x1>=x)&&(x1<=x+14)) && ((y1>=y)&&(y1<=y+13))){  // if inbox frame
   Mstep->databox_click(x1,y1);                         // service member classes
   Mspeed->databox_click(x1,y1);
   Mfr->xbox_click(x1,y1);
   if (step_btn->box_click(x1,y1)){			// 1 = Step motor
    return (1);
   }
  }
  return (0);
 }

 int Motor_direction(){
  return (Mfr->xbox_tf());
 }
 int Motor_speed(){
  return (Mspeed->databox_getint_txt());
 }
 int Motor_steps(){
  return (Mstep->databox_getint_txt());
 }

 int Motor_Step(int latch, int data){

  int i,aint = 0;
  int msb = 0;
  char *binstr = "0000";
  int steps,direction,speed = 0;

  direction = Mfr->xbox_tf();			// get motor data from objs
  speed = Mspeed->databox_getint_txt();
  steps = Mstep->databox_getint_txt();

  for (;steps>0;steps--){
   if (direction)
    cphase++;					// inc\dec current phase
   else
    cphase--;
   if (cphase > 4)
    cphase = 3;
   if (cphase == 4)				// if overflow reset
    cphase = 0;

   aint = motor_lut[cphase];			// store cphase to a int
   if (aint > 8)	                	// if that int is more than 8
    aint /= 16;					// then divide the nybble by 16
   msb = 8;
   for (i=0;i<4;i++){				// convert the int 2
    if ((aint & msb) > 0)			//  a binary string.
     binstr[i] = '1';
    else
     binstr[i] = '0';
    msb /= 2;
   }
   Mphase->databox_text(binstr);		// print the string on the screen.

   aint = data + motor_lut[cphase];             // add the nybbles
   Shift_Out(aint);				// shift the data out
//   gotoxy(40,3);				// print the data
//   cout << "m0 + m1: " << aint << "    ";

   if (latch == 0)
    LE0_Strobe();				// and latch the data.
   else
    LE1_Strobe();
   delay(speed);
  }
  return (motor_lut[cphase]);
 }

// Spin the Motor revol times                   //
 int Motor_Spin(int revol, int latch, int data){

  int msb = 0;
//  char *binstr = "0000";
  int steps,direction,speed,aint = 0;

   direction = Mfr->xbox_tf();			// get motor data from objs
   speed = Mspeed->databox_getint_txt();

  for (;revol>0;revol--){        	        // rotate motor revol X's
   for (steps = spr;steps>0;steps--){           // steps = 1 revolution
    if (direction)
     cphase++;					// inc\dec current phase
    else
     cphase--;
    if (cphase > 4)
     cphase = 3;
    if (cphase == 4)				// if overflow reset
     cphase = 0;

/*    aint = motor_lut[cphase];			// store cphase to a int
    if (aint > 8)	                	// if that int is more than 8
     aint /= 16;					// then divide the nybble by 16
    msb = 8;
    for (i=0;i<4;i++){				// convert the int 2
     if ((aint & msb) > 0)			//  a binary string.
      binstr[i] = '1';
     else
      binstr[i] = '0';
     msb /= 2;
    }
    Mphase->databox_text(binstr);		// print the string on the screen.
*/

    aint = data + motor_lut[cphase];            // add the nybbles
    Shift_Out(aint);				// shift the data out
//    gotoxy(40,3);				// print the data
//    cout << "m0 + m1: " << aint << "    ";

    if (latch == 0)
     LE0_Strobe();				// and latch the data.
    else
     LE1_Strobe();
    delay(speed);
//    delay(speed);
   }
  }
  return (motor_lut[cphase]);
 }

};

