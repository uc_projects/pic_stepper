#include <time.h>
/* port 40h-43h
enviornment:  dos
      base0:  4096
      base1:  18
      base2:  66
*/
void Print_Time_Regs(void){
  clock_t A;
  A = CLK_TCK;

  int base0,       /* buffer for port + 0 */
      base1,       /* buffer for port + 1 */
      base2;       /* buffer for port + 2 */
  int B,C,D,E = 0;           /*       temp          */

  clrscr();
  cout << "num of clk ticks for a secound = " << A << endl;

  for(;!kbhit();){
   base0 = inportb(64);   // read the port
   base1 = inportb(65);
   base2 = inportb(66);
   E = clock();
   if (base0 > B)        // store the highest value for register
   	B = base0;
   if (base1 > C)
        C = base1;
   if (base2 > D)
   	D = base2;

   gotoxy(1,2);
   cout << "clk ticks since app start = " << E << endl;
   cout << "  base0: " << base0 << endl; // print the data
   cout << "   highest#: " << B << endl;
   cout << "  base1: " << base1 << endl;
   cout << "   highest#: " << C << endl;
   cout << "  base2: " << base2 << endl;
   cout << "   highest#: " << D << endl;
  }
  getch();
}
