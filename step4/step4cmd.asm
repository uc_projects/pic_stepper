 title  "F84 + eeprom, stepper commander"
; *********************** Features *********************************************
;  * programmable eeprom storage of speed values & phase tables for each motor.
;
;  * simultanious stepping of motors
;  * command driven, stepping of motors
;
; *******************************************************************************
; Motor Info:
;
;	All Stepper motors have a upper limit on how many pulses the windings can 
; handle per unit of time. To get an accurate estimate of how much time each 
; winding should be energized take the maximum pulses per secound and divide
; that number with a delay constant. Then you can delay X number of times
; before sending another signal to the stepper motor.
;
;   etc. 420PPS\1.04ms = ?
;
;	the Step angle is important to consider also. 360 degrees make a circle.
; motors move in circles. if each step takes 1.8 degrees, then 360\1.8 = 
; ? steps to make a complete revolution.
;
; To get an approximate rpm for a spinning stepper motor you need to know
; the PPS, and the step angle.
;
; 1st find the number of steps for a complete rotation. 
;	360\stepangle = #steps for 1 rpm = srpm.
;
; 2nd * the # steps for 1rpm by the time it takes 1 step to take place.
;	srpm * delay = time for 1 rotation
;
; So if you say you want 25RPM from a stepper motor with a 1.8 degree
; step angle and it takes 100ms per step then it will require:
;	360 \ 1.8 = #steps for 1 rotation = 200
;	200 * .1 = time for 1 rotation = 20 sec.
;
; The maximum # or RPM you can get from this motor is:
;	60sec per min \ 20rps = 3rpm
;
; To find the distance a motor turns with a single step you:
;	2pi * the diameter the rotating shaft \ 1.8degrees
; so when the shaft = 7\8" it moves (2Pi * 7\8) \ 1.8degrees
;
; *******************************************************************************
; Functions
; Step (1-255)times, forward or reverse on any motor.
; Set Speed limits on motors
;
; Function Register:
;  7  6  5  4 -  3  2  1  0  |Bit weights
;  M1 M0 D  S -  I3 I2 I1 I0 |bit symbols
;
; M1&M0   : Motor Select, 1 for mm & 0 For m
; D       : Direction Select, 1 for counter_clockwise & 0 for clockwise
; S       : Step type, 1 = full steps, 0 = half steps
; I3-I0   : Instruction bits
;
; ------------------------------------------------------------------------------- 
; Instruction:       | value      |  Function 
;  set fast speed    |  0000      | speed as a function
;  set slow speed    |  0001      |  of microseconds per step
;  set med speed     |  0010      |
;  step motor        |  0011      | steps a motor x times
;  spin motor        |  0100      | continously steps a motor
;  stop motor        |  0101      | stops a continously  stepping motor
;  port accsess	     |  0111	  | write a nybble to a port(0-3).
;
; *******************************************************************************
DEBUG

  LIST P=16f84, R=DEC
  include p16f84.inc
  __CONFIG _CP_OFF & _WDT_OFF & _XT_OSC & _PWRTE_ON

; *******************************************************************************
; 		Equates
; -------------------------------------------------------------------------------
E	equ 3	; Lcd
D1	equ 1	; Shift Reg. pins
Clk	equ 2
LE0	equ 6
LE1	equ 7

 CBLOCK 12
_w
_status
Count		; Loop reg.
CountHi
Temp		; Temp reg.
TempHi
dly_Lo		; Dly reg.
dly_Hi		; 8 Regs.

RsPos		; cursor postion regs.
KyPos
SPort		; SIPO regs
SCount
LCDTemp		; LCD I\O Reg
DCount		; LCD Disp Regs
DTemp
DTempHi		; 8 Regs

SH0_74573	; shadow registers for latches
SH1_74573	; each Reg holds shadowed values for 2 4bit motors.

BitCount        ;  Count Value for the Bits
Char            ;  Temporary Storage of the Character Read In
RRCount         ;  Loop Count for Reading the Characters
SCCount
Next            ;  Pointer to the Next Value in the Chain
ShadowFSR       ;  Saved the FSR Buffer Value
		; 6 Regs
BufferStart     ;  Character Buffer
Buffer1
Buffer2
Buffer3
Buffer4
Buffer5
Buffer6
BufferEnd	; 8 Regs
 endc

; ------------------------------------------------------------------------------- 
; EEprom Mem map:
;  Address	|   Register		|   Description
;   0000	|   M0 Min spd  	|  Speed values for Motor 0
;   0001	|   M0 defalt spd	|
;   0010	|   M0 Max spd		|
;   0011	|   M0 Phase 1		|  Look up table for motor 0
;   0100	|   M0 Phase 2		|
;   0101	|   M0 Phase 3		|
;   0110	|   M0 Phase 4		|
;   0111	|   M1 Min spd  	|  Speed values for Motor 1
;   1000	|   M1 defalt spd	|
;   1001	|   M1 Max spd		|
;   1010	|   M1 Phase 1		|  Look up table for motor 1
;   1011	|   M1 Phase 2		|
;   1100	|   M1 Phase 3		|
;   1101	|   M1 Phase 4		|
;   1110	|   M2 Min spd  	|  Speed values for Motor 2
;   1111	|   M2 defalt spd	|
; 1-0000	|   M2 Max spd		|
; 1-0001	|   M2 Phase 1		|  Look up table for motor 2
; 1-0010	|   M2 Phase 2		|
; 1-0011	|   M2 Phase 3		|
; 1-0100	|   M2 Phase 4		|
; 1-0101	|   M3 Min spd  	|  Speed values for Motor 3
; 1-0110	|   M3 defalt spd	|
; 1-0111	|   M3 Max spd		|
; 1-1000	|   M3 Phase 1		|  Look up table for motor 3
; 1-1001	|   M3 Phase 2		|
; 1-1010	|   M3 Phase 3		|
; 1-1011	|   M3 Phase 4		|

;
;********************* Program Entry Point **************************************
  org 0
Re_set 	call    Boot
	goto	Root 64.59ms

; *******************************************************************************
; service order = Timer, RB0, PORTB<4:7>, EEPROM
;  notes: GIE is cleared on reset
; -------------------------------------------------------------------------------
  org 4

ISR	movwf	_w		; shadow context regs.
	swapf	STATUS,W
	movwf	_status

RB0_CH	btfsc	INTCON,INTF
	 call	ReadRS232	; Start of Original Serial Read Routine

TMR0_OF	btfss	INTCON,T0IF	; Did timer0 overflow?
         goto   PB0_CH          ; No

PB0_CH	btfss	INTCON,RBIF
         nop

ISR_END	movf	INTCON,W	; clear flags
	andlw	248
	movwf	INTCON

	swapf	_status,W
	movwf	STATUS
	swapf	_w,F
	swapf	_w,W
	retfie

; ***************************************************************************
  include "include\DEBOUNCE.INC"
  include "include\KEYF84.INC"
  include "include\3SEIKO.INC"
  include "include\SERIAL.INC"

; ************************* Look up tables **********************************
LUT	addwf	PCL,f		;cursor ;  Page		; lut address ; line
	dt	"P:   S:   M:"	;0-11	;  Status 0     ;  0-9	      ;  1
	dt	"I:   D:"	;64-70	;		;  10-15      ;  2
	dt	"Instr:"	;0-5	;  Command	;  16-21      ;  1
	dt	"Data:"		;64-68	;		;  22-26      ;  2

; ********************* String look up routine *******************************
; The Cursor must be at location 0 or 64 to display 16 char strings on a 16x2
LUTlp	movwf	TempHi		; CountHi = # chars to display
				; TempHi = starting address of look up table
	movf	TempHi,w	; load the pointer and look up the char!
	call	LUT
	call	LCDChar		; Display the char!
	incf	TempHi,f
	decfsz	CountHi,f
	 goto	$ - 4

; ***************************************************************************
Root
;key assignment:
; 852  ;8 = debug
; 963  ;5 = Command
; 741  ;2 = Status 0 
	call	keypad		; check for key press.
	xorlw	8		; If key = 8 then Debug the os
        btfsc   STATUS,Z        ; if not= skip
	 goto	_Mstat
	movf	DTemp,w		; if key = 2 then goto command display
	xorlw	2
	btfsc	STATUS,Z
	 goto	_Cmd
        goto   Debug84          ; if any other key or no key goto debug

; *********************  Status 0  ******************************************
; *P: F S:FF M:0   * P=phase,S=speed,M=motor
; *I:FF D:FF status* I=instruction,D=Data
; ***************************************************************************
;  Page displayed when rs232 instruction is recieved.
_Mstat	movlw	17		; setup the Motor phase lut's
	movwf	SH0_74573
	call	RetHome		; goto line1
	movlw	12		; Display line1 GUI
	movwf	CountHi
	movlw	0
	call	LUTlp
	movlw	64		; goto line2
	call	LcdAd
	movlw	7		; Display line2 GUI
	movwf	CountHi
	movlw	10
	call	LUTlp

	movf	SH0_74573,w	; lookup M0
	andlw	15
	call	EE_RD
	movwf	Temp

	movf	SH0_74573,w	; lookup M1
	andlw	240
	movwf	TempHi
	swapf	TempHi,W
	call	EE_RD

	addwf	Temp,W
	movwf	SPort
	comf	SPort
	call	SOut		; Send to Motor
	call	SLE0
        goto    _Mstat

; *********************  Command   ******************************************
; *Instr:1111-1111 * change bits in instruction
; * Data:255 snt:x * chage data and press send, which displays a x while busy
; ***************************************************************************
;  Page displayed when a key is pressed, default instr is step m0 15 times
_Cmd	call	RetHome		; goto line1
	movlw	6		; Display line1 GUI
	movwf	CountHi
	movlw	16	
	call	LUTlp
	movlw	64		; goto line2
	call	LcdAd
	movlw	5		; Display line2 GUI
	movwf	CountHi
	movlw	22	
	call	LUTlp
        goto    _Cmd

; *********************   Debug    ******************************************
; *-Rs232 Buffer---* loops around checking the LCD,
; *--Key presses---* rs232 i\O, keypress, and debounce routines.
; ***************************************************************************
Debug84 call    ClrDisp		; Clear the display
        movlw   64              ; Key cursor=64
        movwf   KyPos           
        clrf    RsPos           ; Rs232 cursor=0
        goto    $ + 3

_Dbuglp movf	RsPos,W		; load rs232 cursor
	call	LcdAd
	call	GetCHAR		; Check 4 rs232.
	movwf	TempHi
	xorlw	0
	btfsc	STATUS,Z
	 goto	Dbgkpad		; if no chars in, then test for a keypress

	movf	TempHi,W	; W = character in
	call	LCDChar		; Display char
        movf    TempHi,W        ; W = character in
        call    SendCHAR        ; echo char
	movf	RsPos,W		; test cursor for overflow
	xorlw	15
	btfss	STATUS,Z
	 goto	$ + 4
	clrf	RsPos		; on overflow reset cursor, and pointer
	call	RetHome
	goto	_Dbuglp + 2	; check for another char
	incf	RsPos,F		; if no overflow RSPos + 1
	goto	_Dbuglp	+ 2	; and check for another char

Dbgkpad	movf	KyPos,W		; goto Key cursor on lcd.
	call	LcdAd
	call	keypad		; check for key press.
	xorlw	0
	btfsc	STATUS,Z
	 goto	_Dbuglp		; if no keyress, then check for rs232
        movf    DTemp,w          ; move key to w
	call	DispNum		; Display the key
        movf    DTemp,w        ; Key + 48 = ascii char
	addlw	48
	call	SendCHAR	; Send Key PRESSED via rs232
        incf    KyPos,W         ; do we have an overflow?
	xorlw	80
	btfss	STATUS,Z
	 incf	KyPos,F		; false, Kypos + 1
	btfss	STATUS,Z
	 goto	Dbgkpad + 2	; check for another keypress
	movlw	64		; true, reset cursor & pointer
	movwf	KyPos
	call	LcdAd
	goto	Dbgkpad + 2	; check for another keypress

; *******************************************************************************
; Count_Hi = ((Fhz * Tsec)\3072) + 2
; Time Delay = ((dly_Hi + 770) * 4 * 7)\Fhz
; -------------------------------------------------------------------------------
Dlay5ms	movlw	8		; Delay 5ms
        movwf   dly_Hi
        clrf    dly_Lo
        goto    Delay

Dlay160	movlw	1		; Delay 160us
        movwf   dly_Hi
        movlw   81
        movwf   dly_Lo

Delay				; Delay
  ifndef DEBUG
	decfsz  dly_Lo,F
       	 goto  	Delay
      	decfsz 	dly_Hi,F
       	 goto	Delay      
  endif
      	return

; *******************************************************************************
; 74164 8bit SIPO SR MSB 1st, Output routine.
SOut
  ifndef DEBUG
	movwf   SPort           ; W = 74164 data
        movlw   8               ; Shift 5 bits, RS + 4bit data
        movwf   SCount

SOutLp  bcf     PORTB,D1
        btfsc   SPort,7
         bsf    PORTB,D1

        bsf     PORTB,Clk
        bcf     PORTB,Clk

        rlf     SPort,F
        decfsz  SCount,F
         goto   SOutLp
  endif
	return

SLE0	bcf	PORTB,LE0	; bits are inverted
	bsf	PORTB,LE0
	return

SLE1	bcf	PORTB,LE1		; bits are inverted
	bsf	PORTB,LE1
	return

; *******************************************************************************
; Internal 64 byte EEPROM routines. EEADR = 0-63
; -------------------------------------------------------------------------------
; EEPROM Write, it is assumed the EEDATA reg contians valid data
; and that W = the EEPROM address.
EE_WR	movwf	EEADR			; W = Address to write = EEADR
	bcf	INTCON,GIE		; Disable INTs.

	bsf	STATUS,RP0
	clrf	EECON1 & 0x07F
	bsf	EECON1 & 0x07F,WREN	; Enable Write
	movlw	0x055			;  Do the Standard Write Control.
	movwf	EECON2 & 0x07F
	movlw	0x0AA
	movwf	EECON2 & 0x07F
	bsf	EECON1 & 0x07F,WR	; Begin writing
	bcf	EECON1 & 0x07F,WREN	; Disable any other Write cycle.

EEWlp	clrwdt				; Loop around here until Done
	btfsc	EECON1 & 0x07F,WR	; writing data.
	 goto	EEWlp

	bcf	STATUS,RP0		; enable interrpts
	bsf	INTCON,GIE

EEWV	movf	EEDATA,W		; EEPROM Write Verify.
	movwf	Temp
	movf	EEADR,W
	call	EE_RD			; Read the value written.
	xorwf	Temp,F			; if the values are = then no error.
	btfsc	STATUS,Z
	 retlw	0			; NO EEPROM error.
	retlw	1			; EEPROM error.

; -------------------------------------------------------------------------------
;	movlw	0 - 63			; EE Address to Read
EE_RD	movwf	EEADR			; Addr to read
	bsf	STATUS,RP0		; Bank 1
	bsf    EECON1 & 0x07F,RD	; EE Read
	bcf	STATUS,RP0		; Bank 0
	movf   EEDATA & 0x07F,W		; W = EEDATA
	return

; *******************************************************************************
; Prescaler switching
; -------------------------------------------------------------------------------
WDT_TMR	clrwdt			; clear watchdog timer
	bsf	STATUS,RP0
	movlw	b'11110111'	; Select TMR0, new prescale value and clock source
	movwf	OPTION_REG
	bcf	STATUS,RP0	;Bank 0
	return

TME_WDT	bcf	STATUS,RP0	; Bank 0
	clrf	TMR0		; Clear TMR0

	bsf	STATUS,RP0	; Bank 1
	clrwdt			; clear watchdog timer
	movlw	b'00001000'	; Select WDT, new prescale value and clock source
	movwf	OPTION_REG
	bcf	STATUS,RP0	; Bank 0
	return

; *******************************************************************************
Boot	btfsc	STATUS,NOT_TO	; did wdt reset or wakeup?
	 goto	TO1
	btfsc	STATUS,NOT_PD
	 goto	POR + 2		; wdt_reset
	goto	Ramclr		; wdt_wakeup
TO1	btfss	STATUS,NOT_PD
	 goto	Ramclr		; wakeup from sleep

POR   	clrf	PORTA		; set all outputs Lo
      	clrf	PORTB                      
      	bsf	STATUS,RP0	; goto bank 1
      	movlw	b'00000000'	; RA1=Tx
      	movwf	TRISA & 0X07F
      	movlw	b'11111001'	; RB0=Rx
	movwf	TRISB & 0X07F
        movlw   b'00011111'     ; Setup options
	movwf	OPTION_REG & 0X07F
      	bcf	STATUS, RP0     ; goto bank 0     

Ramclr	btfss	STATUS,NOT_TO
	 goto	intsetp
	btfss	STATUS,NOT_PD
	 goto	intsetp

	movlw	10		; Clear Ram
	movwf	FSR
ilp     clrf    INDF
        incf    FSR,F
        movf    FSR,W
        xorlw   BufferEnd + 1
        btfss   STATUS,Z
         goto   ilp

intsetp
	call	RSinit		; setup int's for RB0
  ifndef DEBUG
	call	Dlay5ms		; initialize LCD
	call	Dlay5ms
	call	Dlay5ms
        call    LCDInit
  endif
	return			; run program!!!
; *******************************************************************************
 end
