'*************************************************************************
'  Codefile: Mice.Bas
'      Type: Mouse utility
'    Author: Tek
'      Date: 6.8.98
'  Codefile: stepper.bas
'      Type: Stepper Motor Controller
'    Author: Brad S.
'      Date:feb 27, 2002
'
'*************************************************************************
DEFINT A-Z
DECLARE FUNCTION MouseInit ()
DECLARE SUB MouseDriver (Ax, Bx, Cx, Dx)
DECLARE SUB MouseHide ()
DECLARE SUB MouseShow ()
DECLARE SUB MouseLoad ()
DECLARE SUB MouseStatus (Lb, Rb, xMouse, yMouse)

DECLARE SUB Delay ()
DECLARE SUB ClickACC ()
DECLARE SUB btoggle (B)
DECLARE SUB InsDecode ()
DECLARE SUB ShiftOut ()
DECLARE SUB clrport ()
DECLARE SUB LPData1 ()
DECLARE SUB LPData0 ()
DECLARE SUB strobe ()
DECLARE SUB LE0Strobe ()
DECLARE SUB LE1Strobe ()

DECLARE SUB GUI ()
DECLARE SUB ClearDataView ()

DIM SHARED Mouse$, Lb, Rb, x, y
'FR,Mot,0, speed\no. steps 8bits
'00000    -  (0000-0000) if FR=1then motor steps coiunter clockwise
DIM SHARED Speed(0 TO 3) AS INTEGER  ' Array for motor speeds
DIM SHARED Steps(0 TO 3) AS INTEGER  ' array for no of steps
DIM SHARED FWRev, LS164, LPTport, SData, DelayTime AS INTEGER
DIM SHARED MotorLut(0 TO 15) AS INTEGER   ' array for Look Up table
DIM SHARED MPtr(0 TO 3) AS INTEGER        'pointers for lookuptable
DIM SHARED pstring AS STRING

'************************* Program Init **********************************
'Delay             ' find out howmany for0to100 loops in a sec.
DelayTime = 20800
CLS               ' Clear the screen
SCREEN 2, 0       ' Switch to mono cga graphics mode
LPTport = 888     ' assign parallel port address

'***************** initialize Motor Data ********************************
MotorLut(0) = 1     ' Motor 0
MotorLut(1) = 4
MotorLut(2) = 2
MotorLut(3) = 8
MotorLut(4) = 16     ' Motor 1
MotorLut(5) = 64
MotorLut(6) = 32
MotorLut(7) = 128


MotorLut(8) = 2      ' Motor 2
MotorLut(9) = 1
MotorLut(10) = 8
MotorLut(11) = 4
MotorLut(12) = 128    ' Motor 3
MotorLut(13) = 64
MotorLut(14) = 32
MotorLut(15) = 16

MPtr(0) = 0           ' pointers for the lut.
MPtr(1) = 4
MPtr(2) = 8
MPtr(3) = 12

Speed(0) = 128           'min speed value
Speed(1) = 128
Speed(2) = 1
Speed(3) = 1

'**************************************************************************
CALL clrport         ' Clear Shift Reg, Latches, & Pins
CALL MouseLoad       ' Load GUI
CALL GUI
LOCATE 14, 3: PRINT "DelayTime = "; DelayTime
CALL MouseShow

'**************************************************************************
   ' File IO

'**************************************************************************
DO WHILE INKEY$ <> CHR$(27)
CALL MouseStatus(Lb, Rb, x, y)
IF Lb OR Rb = -1 THEN                     ' wait for mouse click.
   CALL ClickACC
END IF
LOOP

SCREEN 0
CALL MouseHide                            ' Exit program by Esc key
CALL clrport
CLS
END

'컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
MouseData:
DATA 55,89,E5,8B,5E,0C,8B,07,50,8B,5E,0A,8B,07,50,8B
DATA 5E,08,8B,0F,8B,5E,06,8B,17,5B,58,1E,07,CD,33,53
DATA 8B,5E,0C,89,07,58,8B,5E,0A,89,07,8B,5E,08,89,0F
DATA 8B,5E,06,89,17,5D,CA,08,00

SUB btoggle (B)
'IF y >= 72 AND y <= 78 THEN 'Forward or Reverse 1bit 0-1
   W = FWRev          ' load file
'ELSE
'   W = RSel
'END IF

W = W AND 255     ' ensure 8 bit
IF (W AND 2 ^ B) > 0 THEN 'single out bit
   A = 1
ELSE
   A = 0
END IF

IF y >= 72 AND y <= 78 THEN 'Forward or Reverse 1bit 0-1
   IF A = 1 THEN
      A = 0
      PRINT "0"
   ELSE
      A = 1
      PRINT "1"
   END IF
'ELSE
'   IF A = 1 THEN
'      A = 0
'      PRINT "     "
'   ELSE
'      A = 1
'      PRINT "     "
'   END IF
END IF

'-------------------------------------------------
IF A = 0 THEN     ' sub when clr
   W = W - 2 ^ B
ELSE
   W = W + 2 ^ B  ' add when set
END IF
'------------------------------------------------------------------
IF y >= 72 AND y <= 78 THEN 'Forward or Reverse 1bit 0-1
   FWRev = W
'ELSE
'   RSel = W
END IF
END SUB

SUB ClearDataView
LOCATE 14, 3
PRINT "                                                                             "
LOCATE 15, 3
PRINT "                                                                             "
LOCATE 16, 3
PRINT "                                                                             "
LOCATE 17, 3
PRINT "                                                                             "
LOCATE 18, 3
PRINT "                                                                             "
LOCATE 19, 3
PRINT "                                                                             "
LOCATE 20, 3
PRINT "                                                                             "
LOCATE 21, 3
PRINT "                                                                             "
LOCATE 22, 3
PRINT "                                                                             "
END SUB

SUB ClickACC
CALL MouseStatus(Lb, Rb, x, y)
x = x \ 2
CALL MouseHide 'must hide mouse to print to screen
'first do event for xy values then wait for mouse button to be realeased.
IF y <= 18 AND y >= 5 THEN
   IF x >= 302 AND x <= 316 THEN      'Exit
      SCREEN 0
      CALL clrport
      CLS
      END
   END IF
END IF

' click on a option
'-----------------------------------------------------------------------
IF (x >= 51 AND x <= 54) OR (x >= 68 AND x <= 70) THEN  'mot0 spd-stp
   IF y >= 64 AND y <= 70 THEN 'Speed 0-255
      IF x >= 51 AND x <= 54 THEN
         Speed(0) = Speed(0) + 1
      ELSE
         Speed(0) = Speed(0) - 1
      END IF
      Speed(0) = Speed(0) AND 255
      LOCATE 9, 15
      PRINT "   "
      LOCATE 9, 14
      PRINT Speed(0)
      LOCATE 9, 14
      PRINT "<"
      LOCATE 9, 18
      PRINT ">"
   END IF
   IF y >= 55 AND y <= 62 THEN 'No. Steps 0-255
      IF x >= 51 AND x <= 54 THEN
         Steps(0) = Steps(0) + 1
      ELSE
         Steps(0) = Steps(0) - 1
      END IF
      Steps(0) = Steps(0) AND 255
      LOCATE 8, 15
      PRINT "   "
      LOCATE 8, 14
      PRINT Steps(0)
      LOCATE 8, 14
      PRINT "<"
      LOCATE 8, 18
      PRINT ">"
   END IF
END IF

'-------------------------------------------------------------------------
IF (x >= 127 AND x <= 130) OR (x >= 144 AND x <= 146) THEN  'mot1 spd-stp
   IF y >= 64 AND y <= 70 THEN 'Speed 0-255
      IF x >= 127 AND x <= 130 THEN
         Speed(1) = Speed(1) + 1
      ELSE
         Speed(1) = Speed(1) - 1
      END IF
      Speed(1) = Speed(1) AND 255
      LOCATE 9, 34
      PRINT "   "
      LOCATE 9, 33
      PRINT Speed(1)
      LOCATE 9, 33
      PRINT "<"
      LOCATE 9, 37
      PRINT ">"
   END IF
   IF y >= 55 AND y <= 62 THEN 'No. Steps 0-255
      IF x >= 127 AND x <= 130 THEN
         Steps(1) = Steps(1) + 1
      ELSE
         Steps(1) = Steps(1) - 1
      END IF
      Steps(1) = Steps(1) AND 255
      LOCATE 8, 34
      PRINT "   "
      LOCATE 8, 33
      PRINT Steps(1)
      LOCATE 8, 33
      PRINT "<"
      LOCATE 8, 37
      PRINT ">"
   END IF
END IF

'--------------------------------------------------------------------
IF (x >= 203 AND x <= 206) OR (x >= 220 AND x <= 223) THEN  'mot2 spd-stp
   IF y >= 64 AND y <= 70 THEN 'Speed 0-255
      IF x >= 203 AND x <= 206 THEN
         Speed(2) = Speed(2) + 1
      ELSE
         Speed(2) = Speed(2) - 1
      END IF
      Speed(2) = Speed(2) AND 255
      LOCATE 9, 53
      PRINT "   "
      LOCATE 9, 52
      PRINT Speed(2)
      LOCATE 9, 52
      PRINT "<"
      LOCATE 9, 56
      PRINT ">"
   END IF
   IF y >= 55 AND y <= 62 THEN 'No. Steps 0-255
      IF x >= 203 AND x <= 206 THEN
         Steps(2) = Steps(2) + 1
      ELSE
         Steps(2) = Steps(2) - 1
      END IF
      Steps(2) = Steps(2) AND 255
      LOCATE 8, 53
      PRINT "   "
      LOCATE 8, 52
      PRINT Steps(2)
      LOCATE 8, 52
      PRINT "<"
      LOCATE 8, 56
      PRINT ">"
   END IF
END IF

'-------------------------------------------------------------------------
IF (x >= 296 AND x <= 298) OR (x >= 279 AND x <= 281) THEN  'mot3 spd-stp
   IF y >= 64 AND y <= 70 THEN 'Speed 0-255
      IF x >= 279 AND x <= 281 THEN
         Speed(3) = Speed(3) + 1
      ELSE
         Speed(3) = Speed(3) - 1
      END IF
      Speed(3) = Speed(3) AND 255
      LOCATE 9, 72
      PRINT "   "
      LOCATE 9, 71
      PRINT Speed(3)
      LOCATE 9, 71
      PRINT "<"
      LOCATE 9, 75
      PRINT ">"
   END IF
   IF y >= 55 AND y <= 62 THEN 'No. Steps 0-255
      IF x >= 279 AND x <= 281 THEN
         Steps(3) = Steps(3) + 1
      ELSE
         Steps(3) = Steps(3) - 1
      END IF
      Steps(3) = Steps(3) AND 255
      LOCATE 8, 72
      PRINT "   "
      LOCATE 8, 71
      PRINT Steps(3)
      LOCATE 8, 71
      PRINT "<"
      LOCATE 8, 75
      PRINT ">"
   END IF
END IF

'---------------------------------------------------------------
IF y >= 72 AND y <= 78 THEN 'Forward or Reverse 1bit 0-1
   IF x >= 51 AND x <= 56 THEN
      LOCATE 10, 14
      B = 0
      CALL btoggle(B)
   END IF
   IF x >= 125 AND x <= 130 THEN
      LOCATE 10, 33
      B = 1
      CALL btoggle(B)
   END IF
   IF x >= 202 AND x <= 207 THEN
      LOCATE 10, 52
      B = 2
      CALL btoggle(B)
   END IF
   IF x >= 275 AND x <= 283 THEN
      LOCATE 10, 71
      B = 3
      CALL btoggle(B)
   END IF
END IF

'-----------------------------------------------------------
'IF y >= 79 AND y <= 86 THEN      'Last row
'   IF x >= 51 AND x <= 70 THEN
'      LOCATE 11, 14
'      B = 0
'      CALL btoggle(B)
'   END IF
'   IF x >= 128 AND x <= 146 THEN
'      LOCATE 11, 33
'      B = 1
'      CALL btoggle(B)
'      END IF
'   IF x >= 204 AND x <= 222 THEN
'      LOCATE 11, 52
'      B = 2
'      CALL btoggle(B)
'      END IF
'   IF x >= 280 AND x <= 298 THEN
'      LOCATE 11, 71
'      B = 3
'      CALL btoggle(B)
'      END IF
'END IF
'
'----------------------------------------------------
IF y >= 88 AND y <= 95 THEN      'Step Motor
   LS164 = 0
   IF x >= 23 AND x <= 46 THEN
      W = 0
      IF (RSel AND 1) = 1 THEN  ' prepare motor0
         W = Speed(0) + 512   ' get data byte,set\clr RS
      ELSE
         W = Steps(0)
      END IF
      IF (FWRev AND 1) = 1 THEN
         W = W + 1024         ' set FWRev
      END IF
      LS164 = W
      CALL InsDecode
   END IF
   '------------------------------------------------
   IF x >= 103 AND x <= 126 THEN
      W = 0
      IF (RSel AND 2) = 2 THEN  ' prepare motor1
         W = Speed(1) + 512   ' get data byte,set\clr RS
      ELSE
         W = Steps(1)
      END IF
      IF (FWRev AND 2) = 2 THEN
         W = W + 1024         ' set FWRev
      END IF
      LS164 = W + 2048
      CALL InsDecode
   END IF
   '-------------------------------------------------
   IF x >= 180 AND x <= 202 THEN
      W = 0
      IF (RSel AND 4) > 0 THEN  ' prepare motor2
         W = Speed(2) + 512   ' get data byte,set\clr RS
      ELSE
         W = Steps(2)
      END IF
      IF (FWRev AND 4) > 0 THEN
         W = W + 1024         ' set FWRev
      END IF
      LS164 = W + 4096
      CALL InsDecode
   END IF
   '---------------------------------------------------
   IF x >= 259 AND x <= 282 THEN
      W = 0
      IF (RSel AND 8) > 0 THEN  ' prepare motor3
         W = Speed(3) + 512   ' get data byte,set\clr RS
      ELSE
         W = Steps(3)
      END IF
      IF (FWRev AND 8) > 0 THEN
         W = W + 1024         ' set FWRev
      END IF
      LS164 = W + 4096 + 2048
      CALL InsDecode
   END IF
END IF

'----------------------------------------------------------------
CALL MouseShow                   'show the mouse
DO WHILE Lb OR Rb = -1           'debounce buttons
CALL MouseStatus(Lb, Rb, x, y)
'LOCATE 2, 30: PRINT "Lb="; Lb; "Rb="; Rb; "X:"; x \ 2; "Y:"; y
'Display Coordinates and buttons
LOOP
END SUB

SUB clrport
      OUT LPTport, 7       'min current drain.
      OUT LPTport + 2, 0   'shift out 0x00h
      FOR I = 0 TO 7
         CALL strobe
      NEXT I
      CALL LE0Strobe       'and latch
      CALL LE1Strobe
END SUB

SUB Delay
Test$ = TIME$
Sec$ = RIGHT$(Test$, 2)
A = VAL(Sec$)
A = A + 1
Sec$ = LTRIM$(STR$(A))
MID$(Test$, 7) = Sec$
B = 0
DO WHILE Test$ > TIME$ AND INKEY$ = ""
FOR I = 0 TO 100: NEXT I
B = B + 1
LOOP
DelayTime = B
END SUB

DEFSNG A-Z
SUB GUI
PRINT "�袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴佶袴�"
PRINT "� Stepper Motor Controller                                 By Brad Steffy  쿐sc�"
PRINT "픔컴컴컴컴컴컴컴컴켠컴컴컴컴컴컴컴컴컴爐컴컴컴컴컴컴컴컴켠컴컴컴컴컴컴컴컴컨컴캘"
PRINT "�                  �                  �                  �                     �"
PRINT "�                  �                  �                  �                     �"
PRINT "�      Motor0      �      Motor1      �      Motor2      �       Motor3        �"
PRINT "� Bin Output:0001  � Bin Output:0001  � Bin Output:0001  � Bin Output:0001     �"
PRINT "�  No. Steps:<0  > �  No. Steps:<0  > �  No. Steps:<0  > �  No. Steps:<0  >    �"
PRINT "�      Speed:<128> �      Speed:<128> �      Speed:<0  > �      Speed:<0  >    �"
PRINT "�        F\R:0     �        F\R:0     �        F\R:0     �        F\R:0        �"
PRINT "�                  �                  �                  �                     �"
PRINT "�     <Step>       �      <Step>      �      <Step>      �       <Step>        �"
PRINT "픔컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴懃컴컴컴컴컴컴컴컴켕컴컴컴컴컴컴컴컴컴컴캘"
PRINT "�                                                                              �"
PRINT "�                                                                              �"
PRINT "�                                                                              �"
PRINT "�                                                                              �"
PRINT "�                                                                              �"
PRINT "�                                                                              �"
PRINT "�                                                                              �"
PRINT "�                                                                              �"
PRINT "�                                                                              �"
PRINT "훤袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴暠"
END SUB

DEFINT A-Z
SUB InsDecode
'LUT_Motor(0-3) 'storage for look up table values.
IF (LS164 AND 512) = 0 THEN
   FOR I = 0 TO (LS164 AND 255)
      IF (LS164 AND 6144) = 0 THEN  ' Motor 0 (0-3)
         IF (LS164 AND 1024) = 0 THEN
            MPtr(0) = MPtr(0) + 1   ' Motor 0 Counter Clockwise
            IF MPtr(0) = 4 THEN
               MPtr(0) = 0
            END IF
         ELSE
            MPtr(0) = MPtr(0) - 1   ' Motor 0 Clockwise
            IF MPtr(0) < 0 THEN
               MPtr(0) = 3
            END IF
         END IF
         SData = (MotorLut(MPtr(0))) + (MotorLut(MPtr(1)))
         CALL ShiftOut
         CALL LE0Strobe
         LOCATE 7, 14
         PRINT "    "
         LOCATE 7, 14
         FOR V = 0 TO (Speed(0) * 128): NEXT V
      END IF
      IF (LS164 AND 6144) = 2048 THEN  ' Motor 1 (4-7)
         IF (LS164 AND 1024) = 0 THEN
            MPtr(1) = MPtr(1) + 1   ' Motor 1 Counter Clockwise
            IF MPtr(1) = 8 THEN
               MPtr(1) = 4
            END IF
         ELSE
            MPtr(1) = MPtr(1) - 1   ' Motor 1 Clockwise
            IF MPtr(1) = 3 THEN
               MPtr(1) = 7
            END IF
         END IF
         SData = (MotorLut(MPtr(0))) + (MotorLut(MPtr(1)))
         CALL ShiftOut
         CALL LE0Strobe
         LOCATE 7, 33
         PRINT "    "
         LOCATE 7, 33
         FOR V = 0 TO (Speed(1) * 128): NEXT V
      END IF

      IF (LS164 AND 6144) = 4096 THEN  ' Motor 2 (8-11)
         IF (LS164 AND 1024) = 0 THEN
            MPtr(2) = MPtr(2) + 1   ' Motor 2 Counter Clockwise
            IF MPtr(2) = 12 THEN
               MPtr(2) = 8
            END IF
         ELSE
            MPtr(2) = MPtr(2) - 1   ' Motor 2 Clockwise
            IF MPtr(2) = 7 THEN
               MPtr(2) = 11
            END IF
         END IF
         SData = (MotorLut(MPtr(2))) + (MotorLut(MPtr(3)))
         CALL ShiftOut
         CALL LE1Strobe
         LOCATE 7, 52
         PRINT "    "
         LOCATE 7, 52
      END IF
      IF (LS164 AND 6144) = 6144 THEN  ' Motor 3 (12-15)
         IF (LS164 AND 1024) = 0 THEN
            MPtr(3) = MPtr(3) + 1   ' Motor 3 Counter Clockwise
            IF MPtr(3) = 16 THEN
               MPtr(3) = 12
            END IF
         ELSE
            MPtr(3) = MPtr(3) - 1   ' Motor 3 Clockwise
            IF MPtr(3) = 11 THEN
               MPtr(3) = 15
            END IF
         END IF
         SData = (MotorLut(MPtr(2))) + (MotorLut(MPtr(3)))
         CALL ShiftOut
         CALL LE1Strobe
         LOCATE 7, 71
         PRINT "    "
         LOCATE 7, 71
      END IF
   
      pstring = ""
      IF (LS164 AND 6144) = 0 OR (LS164 AND 6144) = 4096 THEN
         A = 8                    'bIN2dEC ROUTINE for Phase in LSNybble
         FOR B = 0 TO 3
            IF (SData AND A) = A THEN
               pstring = pstring + "1"
            ELSE
               pstring = pstring + "0"
            END IF
            A = A \ 2
         NEXT B
      ELSE
         A = 128                    'bIN2dEC ROUTINE for Phase in MSNybble
         FOR B = 0 TO 3
            IF (SData AND A) = A THEN
               pstring = pstring + "1"
            ELSE
               pstring = pstring + "0"
            END IF
            A = A \ 2
         NEXT B
      END IF
      PRINT pstring

        'pstring = ""
        ' A = 4096                    'bIN2dEC ROUTINE for instruction debug
        ' FOR B = 0 TO 12
        '    IF (LS164 AND A) = A THEN
        '       pstring = pstring + "1"
        '    ELSE
        '       pstring = pstring + "0"
        '    END IF
        '    A = A \ 2
        ' NEXT B
        ' LOCATE 14, 3
        ' PRINT "insdecode LS164: "; pstring
   NEXT I
END IF
END SUB

SUB LE0Strobe
OUT LPTport, 5
OUT LPTport, 7
END SUB

SUB LE1Strobe
OUT LPTport, 3
OUT LPTport, 7
END SUB

SUB LPData0
   pstring = pstring + "0"
   OUT LPTport, 7
END SUB

SUB LPData1
   pstring = pstring + "1"
   OUT LPTport, 6
END SUB

SUB MouseDriver (Ax, Bx, Cx, Dx)
 
DEF SEG = VARSEG(Mouse$)
Mouse = SADD(Mouse$)
CALL Absolute(Ax, Bx, Cx, Dx, Mouse)

END SUB

SUB MouseHide

Ax = 2
MouseDriver Ax, 0, 0, 0

END SUB

FUNCTION MouseInit
 
Ax = 0
MouseDriver Ax, 0, 0, 0
MouseInit = Ax

END FUNCTION

SUB MouseLoad

Mouse$ = SPACE$(57)

RESTORE MouseData

FOR I = 1 TO 57
 READ A$
 H$ = CHR$(VAL("&H" + A$))
 MID$(Mouse$, I, 1) = H$
NEXT I

IF NOT MouseInit THEN
 PRINT "Mouse not found."
 END
END IF

END SUB

SUB MouseShow
 
Ax = 1
MouseDriver Ax, 0, 0, 0

END SUB

SUB MouseStatus (Lb, Rb, xMouse, yMouse)
 
Ax = 3
MouseDriver Ax, Bx, Cx, Dx
Lb = ((Bx AND 1) <> 0)
Rb = ((Bx AND 2) <> 0)
xMouse = Cx
yMouse = Dx

END SUB

SUB ShiftOut
'SData = shift reg output data
   pstring = ""
   IF (SData AND 128) = 128 THEN
      CALL LPData1
   ELSE
      CALL LPData0
   END IF
   CALL strobe
   IF (SData AND 64) = 64 THEN
      CALL LPData1
   ELSE
      CALL LPData0
   END IF
   CALL strobe
   IF (SData AND 32) = 32 THEN
      CALL LPData1
   ELSE
      CALL LPData0
   END IF
   CALL strobe
   IF (SData AND 16) = 16 THEN
      CALL LPData1
   ELSE
      CALL LPData0
   END IF
   CALL strobe

   IF (SData AND 8) = 8 THEN
      CALL LPData1
   ELSE
      CALL LPData0
   END IF
   CALL strobe
   IF (SData AND 4) = 4 THEN
      CALL LPData1
   ELSE
      CALL LPData0
   END IF
   CALL strobe
   IF (SData AND 2) = 2 THEN
      CALL LPData1
   ELSE
      CALL LPData0
   END IF
   CALL strobe
   IF (SData AND 1) = 1 THEN
      CALL LPData1
   ELSE
      CALL LPData0
   END IF
   CALL strobe

'   LOCATE 16, 3
'   PRINT "Shift out: "; pstring
END SUB

SUB strobe
OUT LPTport + 2, 1
OUT LPTport + 2, 0
FOR I = 0 TO DelayTime: NEXT I
END SUB

